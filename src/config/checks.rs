// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::error::Error as StdError;

use erased_serde::Deserializer;
use git_checks_config::{BranchCheckConfig, CommitCheckConfig, TopicCheckConfig};
use git_checks_core::{BranchCheck, Check, GitCheckConfiguration, TopicCheck};
use serde_json::Value;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum CheckError {
    #[error("failed to parse {} config: {}", kind, source)]
    Parse {
        kind: String,
        #[source]
        source: Box<dyn StdError + Send + Sync>,
    },
    #[error("unknown check: {}", kind)]
    UnknownCheck { kind: String },
}

impl CheckError {
    fn parse<K>(kind: K, source: Box<dyn StdError + Send + Sync>) -> Self
    where
        K: Into<String>,
    {
        CheckError::Parse {
            kind: kind.into(),
            source,
        }
    }

    fn unknown_check<K>(kind: K) -> Self
    where
        K: Into<String>,
    {
        CheckError::UnknownCheck {
            kind: kind.into(),
        }
    }
}

/// A collection of checks.
#[derive(Default)]
pub struct Checks {
    /// Per-commit checks.
    commit_checks: Vec<Box<dyn Check>>,
    /// Per-branch checks.
    branch_checks: Vec<Box<dyn BranchCheck>>,
    /// Per-topic checks.
    topic_checks: Vec<Box<dyn TopicCheck>>,
}

impl Checks {
    /// Create an empty set of checks.
    pub fn new() -> Self {
        Checks {
            commit_checks: Vec::new(),
            branch_checks: Vec::new(),
            topic_checks: Vec::new(),
        }
    }

    /// Add a check to the collection.
    pub fn add_check(&mut self, kind: &str, config: Value) -> Result<(), CheckError> {
        let mut deserializer = <dyn Deserializer>::erase(config);

        for check in git_checks_config::inventory::iter::<BranchCheckConfig> {
            if check.name() == kind {
                let check = check
                    .create(&mut deserializer)
                    .map_err(|err| CheckError::parse(kind, err))?;
                self.branch_checks.push(check);
                return Ok(());
            }
        }

        for check in git_checks_config::inventory::iter::<CommitCheckConfig> {
            if check.name() == kind {
                let check = check
                    .create(&mut deserializer)
                    .map_err(|err| CheckError::parse(kind, err))?;
                self.commit_checks.push(check);
                return Ok(());
            }
        }

        for check in git_checks_config::inventory::iter::<TopicCheckConfig> {
            if check.name() == kind {
                let check = check
                    .create(&mut deserializer)
                    .map_err(|err| CheckError::parse(kind, err))?;
                self.topic_checks.push(check);
                return Ok(());
            }
        }

        Err(CheckError::unknown_check(kind))
    }

    /// Whether there are any checks or not.
    pub fn is_empty(&self) -> bool {
        self.commit_checks.is_empty()
            && self.branch_checks.is_empty()
            && self.topic_checks.is_empty()
    }

    /// Get the configuration for the checks.
    pub fn config(&self) -> GitCheckConfiguration {
        let mut conf = GitCheckConfiguration::new();

        for check in &self.commit_checks {
            conf.add_check(check.as_ref());
        }

        for check in &self.branch_checks {
            conf.add_branch_check(check.as_ref());
        }

        for check in &self.topic_checks {
            conf.add_topic_check(check.as_ref());
        }

        conf
    }
}

#[cfg(test)]
mod tests {
    use serde_json::json;

    use crate::config::checks::{CheckError, Checks};

    #[test]
    fn test_checks_exist() {
        let check_names = [
            "allow_robot",
            "bad_commit",
            "bad_commit/topic",
            "bad_commits",
            "bad_commits/topic",
            "changelog",
            "changelog/topic",
            "check_end_of_line",
            "check_end_of_line/topic",
            "check_executable_permissions",
            "check_executable_permissions/topic",
            "check_size",
            "check_size/topic",
            "check_whitespace",
            "check_whitespace/topic",
            "commit_subject",
            "formatting",
            "formatting/topic",
            "invalid_paths",
            "invalid_paths/topic",
            "invalid_utf8",
            "invalid_utf8/topic",
            "lfs_pointer",
            "lfs_pointer/topic",
            "reject_binaries",
            "reject_binaries/topic",
            "reject_conflict_paths",
            "reject_conflict_paths/topic",
            "reject_merges",
            "reject_separate_root",
            "reject_symlinks",
            "reject_symlinks/topic",
            "release_branch",
            "restricted_path",
            "restricted_path/topic",
            "submodule_available",
            "submodule_rewind",
            "submodule_watch",
            "third_party",
            "valid_name",
        ];

        for &name in check_names.iter() {
            let mut checks = Checks::new();
            if let Err(CheckError::UnknownCheck {
                kind,
            }) = checks.add_check(name, json!({}))
            {
                assert_eq!(kind, name);
                panic!("the '{}' check should be supported", kind);
            }
        }
    }
}
