// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use chrono::{DateTime, Utc};
use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub enum CheckAction {
    #[serde(rename = "created")]
    Created,
    #[serde(rename = "requested")]
    Requested,
    #[serde(rename = "rerequested")]
    Rerequested,
    #[serde(rename = "requested_action")]
    RequestedAction,
    #[serde(rename = "completed")]
    Completed,
}

#[derive(Debug, Deserialize)]
pub struct CheckRunHookAttrs {
    pub name: String,
}

#[derive(Debug, Deserialize)]
pub struct BranchHookAttrs {
    pub repo: Option<RepositoryHookAttrs>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum PullRequestState {
    #[serde(rename = "open")]
    Open,
    #[serde(rename = "closed")]
    Closed,
    #[serde(rename = "merged")]
    Merged,
}

#[derive(Debug, Deserialize)]
pub struct PullRequestHookAttrs {
    pub number: u64,
    pub base: BranchHookAttrs,
    pub state: PullRequestState,
    pub merged: Option<bool>,
    pub updated_at: DateTime<Utc>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum AppAction {
    #[serde(rename = "revoked")]
    Revoked,
    // TODO
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum InstallationAction {
    #[serde(rename = "created")]
    Created,
    #[serde(rename = "deleted")]
    Deleted,
}

#[derive(Debug, Deserialize)]
pub struct UserHookAttrs {
    pub login: String,
}

#[derive(Debug, Deserialize)]
pub struct InstallationHookAttrs {
    pub id: u64,
    pub account: UserHookAttrs,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum InstallationRepositoriesAction {
    #[serde(rename = "added")]
    Added,
    #[serde(rename = "removed")]
    Removed,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum RepositorySelection {
    #[serde(rename = "selected")]
    Selected,
    #[serde(rename = "all")]
    All,
}

#[derive(Debug, Deserialize)]
pub struct RepositoryHookAttrs {
    pub full_name: String,
}

#[derive(Debug, Deserialize)]
pub struct IssuePullRequestAttrs {}

#[derive(Debug, Deserialize)]
pub struct IssueHookAttrs {
    pub number: u64,
    pub pull_request: Option<IssuePullRequestAttrs>,
}

#[derive(Debug, Deserialize)]
pub struct CommentHookAttrs {
    pub node_id: String,
    pub user: UserHookAttrs,
    pub body: String,
    pub created_at: DateTime<Utc>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum MemberAction {
    #[serde(rename = "added")]
    Added,
    #[serde(rename = "edited")]
    Edited,
    #[serde(rename = "deleted")]
    Deleted,
    #[serde(rename = "removed")]
    Removed,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum Permission {
    #[serde(rename = "admin")]
    Admin,
    #[serde(rename = "write")]
    Write,
    #[serde(rename = "read")]
    Read,
    #[serde(rename = "maintain")]
    Maintain,
    // TODO
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum OrganizationAction {
    #[serde(rename = "member_added")]
    MemberAdded,
    #[serde(rename = "member_removed")]
    MemberRemoved,
    #[serde(rename = "member_invited")]
    MemberInvited,
    #[serde(rename = "deleted")]
    Deleted,
}

#[derive(Debug, Deserialize)]
pub struct OrganizationHookAttrs {
    pub login: String,
}

#[derive(Debug, Deserialize)]
pub struct PullRequestReviewHookAttrs {
    pub body: Option<String>,
    pub submitted_at: DateTime<Utc>,
    pub user: UserHookAttrs,
    pub node_id: String,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum PullRequestAction {
    #[serde(rename = "assigned")]
    Assigned,
    #[serde(rename = "unassigned")]
    Unassigned,
    #[serde(rename = "converted_to_draft")]
    ConvertedToDraft,
    #[serde(rename = "review_requested")]
    ReviewRequested,
    #[serde(rename = "review_request_removed")]
    ReviewRequestRemoved,
    #[serde(rename = "labeled")]
    Labeled,
    #[serde(rename = "unlabeled")]
    Unlabeled,
    #[serde(rename = "opened")]
    Opened,
    #[serde(rename = "edited")]
    Edited,
    #[serde(rename = "closed")]
    Closed,
    #[serde(rename = "reopened")]
    Reopened,
    #[serde(rename = "synchronize")]
    Synchronize,
    #[serde(rename = "ready_for_review")]
    ReadyForReview,
    #[serde(rename = "milestoned")]
    Milestoned,
    #[serde(rename = "demilestoned")]
    Demilestoned,
}

#[derive(Debug, Deserialize)]
pub struct CheckRunEvent {
    pub action: CheckAction,
    pub check_run: CheckRunHookAttrs,
    pub pull_requests: Option<Vec<PullRequestHookAttrs>>,
}

#[derive(Debug, Deserialize)]
pub struct CheckSuiteEvent {
    pub action: CheckAction,
}

#[derive(Debug, Deserialize)]
pub struct GithubAppAuthorizationEvent {
    pub action: AppAction,
}

#[derive(Debug, Deserialize)]
pub struct InstallationEvent {
    pub action: InstallationAction,
    pub installation: InstallationHookAttrs,
}

#[derive(Debug, Deserialize)]
pub struct InstallationRepositoriesEvent {
    pub action: InstallationRepositoriesAction,
    pub installation: InstallationHookAttrs,
    pub repository_selection: RepositorySelection,
    pub repositories_added: Vec<RepositoryHookAttrs>,
    pub repositories_removed: Vec<RepositoryHookAttrs>,
}

#[derive(Debug, Deserialize)]
pub struct IssueCommentEvent {
    pub issue: IssueHookAttrs,
    pub comment: CommentHookAttrs,
    pub repository: RepositoryHookAttrs,
}

#[derive(Debug, Deserialize)]
pub struct MemberEvent {
    pub repository: RepositoryHookAttrs,
}

#[derive(Debug, Deserialize)]
pub struct MembershipEvent {
    pub organization: OrganizationHookAttrs,
}

#[derive(Debug, Deserialize)]
pub struct OrganizationEvent {
    pub action: OrganizationAction,
    pub organization: OrganizationHookAttrs,
}

#[derive(Debug, Deserialize)]
pub struct PullRequestReviewEvent {
    pub pull_request: PullRequestHookAttrs,
    pub review: PullRequestReviewHookAttrs,
}

#[derive(Debug, Deserialize)]
pub struct PullRequestEvent {
    pub after: Option<String>,
    pub action: PullRequestAction,
    pub pull_request: PullRequestHookAttrs,
}

#[derive(Debug, Deserialize)]
pub struct PushEvent {
    #[serde(rename = "ref")]
    pub ref_: String,
    pub after: String,
    pub repository: RepositoryHookAttrs,
    pub sender: UserHookAttrs,
}

#[derive(Debug, Deserialize)]
pub struct TeamEvent {
    pub repository: Option<RepositoryHookAttrs>,
    pub organization: Option<OrganizationHookAttrs>,
}

#[derive(Debug, Deserialize)]
pub struct TeamAddEvent {
    pub repository: RepositoryHookAttrs,
}
