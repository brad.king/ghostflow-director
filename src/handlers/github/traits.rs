// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::borrow::Cow;

use chrono::Utc;
use ghostflow::host::{Comment, Commit, HostingService, HostingServiceError};
use ghostflow_github::GithubService;
use git_workarea::CommitId;
use graphql_client::GraphQLQuery;
use log::warn;
use thiserror::Error;

use crate::actions::merge_requests::Info;
use crate::ghostflow_ext::DirectorHostingService;
use crate::handlers::common::data::*;
use crate::handlers::github::hooks::*;
use crate::handlers::github::queries;

#[derive(Debug, Error)]
pub enum GithubMergeRequestError {
    #[error(
        "desynchronized database, MR {}#{}, claims commit {}, but hook claims commit {}",
        project,
        id,
        commit,
        hook_commit
    )]
    Desync {
        project: String,
        id: u64,
        commit: CommitId,
        hook_commit: CommitId,
    },
    #[error("deleted base repository")]
    DeletedRepo,
}

impl GithubMergeRequestError {
    fn desync(project: String, id: u64, commit: CommitId, hook_commit: CommitId) -> Self {
        GithubMergeRequestError::Desync {
            project,
            id,
            commit,
            hook_commit,
        }
    }
}

/// Information for a Github merge request.
pub struct GithubMergeRequestInfo;

impl GithubMergeRequestInfo {
    /// Create a merge request information block from a webhook.
    pub fn from_web_hook<'a>(
        service: &dyn DirectorHostingService,
        hook: &'a PullRequestEvent,
    ) -> Result<Result<Info<'a>, GithubMergeRequestError>, HostingServiceError> {
        let base_repo_name = if let Some(repo) = hook.pull_request.base.repo.as_ref() {
            &repo.full_name
        } else {
            return Ok(Err(GithubMergeRequestError::DeletedRepo));
        };
        let is_open = match hook.pull_request.state {
            PullRequestState::Open => true,
            PullRequestState::Closed | PullRequestState::Merged => false,
        };
        let mr = service.merge_request(base_repo_name, hook.pull_request.number)?;

        // It has been observed that on PR updates, the hook's new ref for the PR is not yet
        // represented in the database backing the GraphQL queries we do to get the MR abstraction
        // used here. Sanity check that the `after` field, only present for `synchronize` PR
        // actions, it matches what we got from the GraphQL query. If it does not match, we will
        // defer the event and try it again later.
        if let Some(ref after) = hook.after {
            if mr.commit.id.as_str() != after {
                return Ok(Err(GithubMergeRequestError::desync(
                    mr.target_repo.name,
                    mr.id,
                    mr.commit.id,
                    CommitId::new(after),
                )));
            }
        }

        Ok(Ok(Info {
            merge_request: Cow::Owned(mr),
            was_opened: hook.action == PullRequestAction::Opened
                || hook.action == PullRequestAction::Reopened,
            was_merged: hook.action == PullRequestAction::Closed
                && hook.pull_request.merged.unwrap_or(false),
            is_open,
            date: hook.pull_request.updated_at,
        }))
    }
}

#[derive(Debug, Error)]
enum PullInfoError {
    #[error("no repository for {}", repo)]
    NoRepository { repo: String },
    #[error("no pull request for {}#{}", repo, number)]
    NoPullRequest { repo: String, number: u64 },
}

impl PullInfoError {
    fn no_repository(repo: String) -> Self {
        PullInfoError::NoRepository {
            repo,
        }
    }

    fn no_pull_request(repo: String, number: u64) -> Self {
        PullInfoError::NoPullRequest {
            repo,
            number,
        }
    }
}

/// Information for a Github merge request comment.
pub struct GithubMergeRequestNoteInfo;

impl GithubMergeRequestNoteInfo {
    /// Create a merge request comment information block from a webhook.
    pub fn from_web_hook<'a>(
        service: &GithubService,
        hook: &'a IssueCommentEvent,
    ) -> Result<Option<MergeRequestNoteInfo<'a>>, HostingServiceError> {
        if hook.issue.pull_request.is_none() {
            return Ok(None);
        }

        let note = Comment {
            id: hook.comment.node_id.clone(),
            is_system: false, // XXX(github): Do we get these?
            is_branch_update: false,
            created_at: hook.comment.created_at,
            author: service.user(&hook.repository.full_name, &hook.comment.user.login)?,
            content: hook.comment.body.clone(),
        };

        let mr = service.merge_request(&hook.repository.full_name, hook.issue.number)?;

        let (owner, name) = GithubService::split_project(&hook.repository.full_name)?;
        let vars = queries::pull_request_info::Variables {
            owner: owner.into(),
            name: name.into(),
            pull: hook.issue.number as i64,
        };
        let query = queries::PullRequestInfo::build_query(vars);
        let pr_info = service
            .github()
            .send::<queries::PullRequestInfo>(owner, &query)
            .map_err(HostingServiceError::host)
            .and_then(|rsp| {
                GithubService::check_rate_limits(
                    &rsp.rate_limit_info.rate_limit,
                    queries::PullRequestInfo::name(),
                );
                rsp.repository
                    .ok_or_else(|| PullInfoError::no_repository(hook.repository.full_name.clone()))
                    .map_err(HostingServiceError::host)
            })
            .and_then(|rsp| {
                rsp.pull_request
                    .ok_or_else(|| {
                        PullInfoError::no_pull_request(
                            hook.repository.full_name.clone(),
                            hook.issue.number,
                        )
                    })
                    .map_err(HostingServiceError::host)
            })?;
        let is_open = match pr_info.state {
            queries::pull_request_info::PullRequestState::OPEN => true,
            queries::pull_request_info::PullRequestState::CLOSED
            | queries::pull_request_info::PullRequestState::MERGED => false,
            queries::pull_request_info::PullRequestState::Other(s) => {
                warn!(
                    target: "github",
                    "unknown pull request state: {}",
                    s,
                );
                true
            },
        };
        let mr_info = Info {
            merge_request: Cow::Owned(mr),
            was_opened: false,
            was_merged: false,
            is_open,
            date: pr_info.updated_at,
        };

        Ok(Some(MergeRequestNoteInfo {
            merge_request: mr_info,
            note,
        }))
    }

    /// Create a merge request comment information block from a webhook.
    pub fn from_web_hook_review<'a>(
        service: &GithubService,
        hook: &'a PullRequestReviewEvent,
    ) -> Result<Result<MergeRequestNoteInfo<'a>, GithubMergeRequestError>, HostingServiceError>
    {
        let base_repo_name = if let Some(repo) = hook.pull_request.base.repo.as_ref() {
            &repo.full_name
        } else {
            return Ok(Err(GithubMergeRequestError::DeletedRepo));
        };
        let note = Comment {
            id: hook.review.node_id.clone(),
            is_system: false,
            is_branch_update: false,
            created_at: hook.review.submitted_at,
            author: service.user(base_repo_name, &hook.review.user.login)?,
            content: hook.review.body.clone().unwrap_or_default(),
        };
        let mr = service.merge_request(base_repo_name, hook.pull_request.number)?;
        let is_open = match hook.pull_request.state {
            PullRequestState::Open => true,
            PullRequestState::Closed | PullRequestState::Merged => false,
        };
        let mr_info = Info {
            merge_request: Cow::Owned(mr),
            was_opened: false,
            was_merged: false,
            is_open,
            date: hook.pull_request.updated_at,
        };

        Ok(Ok(MergeRequestNoteInfo {
            merge_request: mr_info,
            note,
        }))
    }
}

/// Information for a Github push.
pub struct GithubPushInfo;

impl GithubPushInfo {
    /// Create a push information block from a webhook.
    pub fn from_web_hook(
        service: &dyn DirectorHostingService,
        hook: PushEvent,
    ) -> Result<PushInfo, HostingServiceError> {
        let commit_id = CommitId::new(&hook.after);
        let last_pipeline = if commit_id.as_str() == "0000000000000000000000000000000000000000" {
            None
        } else {
            service
                .commit(&hook.repository.full_name, &commit_id)?
                .last_pipeline
        };

        Ok(PushInfo {
            commit: Commit {
                repo: service.repo(&hook.repository.full_name)?,
                refname: Some(hook.ref_),
                id: commit_id,
                last_pipeline,
            },
            author: service.user(&hook.repository.full_name, &hook.sender.login)?,
            // XXX(github): GitHub does not send the time of the push to us.
            // https://platform.github.community/t/pushevent-webhook-is-missing-when-the-push-occurred/7337
            date: Utc::now(),
        })
    }
}
