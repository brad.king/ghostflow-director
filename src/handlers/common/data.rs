// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Common data structures required for the workflow.
//!
//! This module contains structures necessary to carry out the actual actions wanted for the
//! requested branches.

use chrono::{DateTime, Utc};
use ghostflow::host::{Comment, Commit, Repo, User};

use crate::actions::merge_requests::Info;
use crate::ghostflow_ext::Membership;

/// Information required to handle a push event.
#[derive(Debug, Clone)]
pub struct PushInfo {
    /// The commit being pushed.
    pub commit: Commit,
    /// The user who pushed the commit.
    pub author: User,
    /// When the push occurred.
    pub date: DateTime<Utc>,
}

/// Information required to handle a note on a merge request.
#[derive(Debug, Clone)]
pub struct MergeRequestNoteInfo<'a> {
    /// The merge request.
    pub merge_request: Info<'a>,
    /// The note.
    pub note: Comment,
}

/// Information required to handle the addition of a user to a project.
#[derive(Debug, Clone)]
pub struct MembershipAdditionInfo {
    /// The repository.
    pub repo: Repo,
    /// The membership.
    pub membership: Membership,
}

/// Information required to handle the removal of a user from a project.
#[derive(Debug, Clone)]
pub struct MembershipRemovalInfo {
    /// The repository.
    pub repo: Repo,
    /// The user.
    pub user: User,
}
