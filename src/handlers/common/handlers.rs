// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Core handler logic for kitware workflow actions.
//!
//! This module contains functions which carry out the actual actions wanted for the requested
//! branches.

#[macro_use]
pub(crate) mod utils;

mod jobs;
mod membership;
mod merge_requests;
mod notes;
mod projects;
mod push;

pub use self::jobs::handle_clear_test_refs;
pub use self::jobs::handle_stage_tag;
pub use self::jobs::handle_update_follow_refs;
pub use self::membership::handle_group_membership_refresh;
pub use self::membership::handle_project_membership_refresh;
pub use self::merge_requests::handle_merge_request_update;
pub use self::notes::handle_merge_request_note;
pub use self::projects::handle_project_creation;
pub use self::push::handle_push;
