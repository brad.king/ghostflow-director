// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::iter;
use std::thread;
use std::time::Duration;

use ghostflow::actions::check::{CheckError, CheckStatus};
use ghostflow::actions::dashboard::Dashboard;
use ghostflow::host::{HostingService, HostingServiceError, MergeRequest};
use git_workarea::GitContext;
use itertools::Itertools;
use json_job_dispatch::JobResult;
use log::{error, info, warn};
use serde_json::{json, Value};
use thiserror::Error;

use crate::actions::merge_requests::Info;
use crate::config::{
    Branch, Host, Project, StageAction, StageUpdatePolicy, TestAction, TestBackend,
};
use crate::handlers::common::handlers::utils;

// The maximum number of times we will retry server errors.
const BACKOFF_LIMIT: usize = 3;
// The number of seconds to start retries at.
const BACKOFF_START: Duration = Duration::from_secs(1);
// How much to scale retry timeouts for a single query.
const BACKOFF_SCALE: u32 = 2;

#[derive(Debug, Error)]
enum BackoffError {
    #[error("no result after exponential backoff")]
    NoResult {},
}

impl BackoffError {
    fn no_result() -> Self {
        BackoffError::NoResult {}
    }
}

fn retry_with_backoff<F, K>(desc: String, mut tryf: F) -> Result<K, HostingServiceError>
where
    F: FnMut() -> Result<K, HostingServiceError>,
{
    iter::repeat(())
        .take(BACKOFF_LIMIT)
        .scan(BACKOFF_START, |timeout, _| {
            match tryf() {
                Ok(r) => Some(Some(Ok(r))),
                Err(err) => {
                    info!(
                        target: "ghostflow-director/handler",
                        "failed attempt to {} (retrying after {:?}): {:?}",
                        desc, timeout, err,
                    );
                    if let HostingServiceError::Fetch {
                        ..
                    } = err
                    {
                        thread::sleep(*timeout);
                        *timeout *= BACKOFF_SCALE;
                        Some(None)
                    } else {
                        Some(Some(Err(err)))
                    }
                },
            }
        })
        .flatten()
        .next()
        .unwrap_or_else(|| Err(HostingServiceError::host(BackoffError::no_result())))
}

struct MergeRequestUpdateResult;

impl MergeRequestUpdateResult {
    fn closed(branch: &str) -> JobResult {
        JobResult::reject(format!(
            "the merge request for the branch {} has been closed",
            branch,
        ))
    }
}

/// Handle an update to a merge request.
pub fn handle_merge_request_update(data: &Value, host: &Host, mr: &Info) -> JobResult {
    let (project, branch) = try_action!(utils::get_branch(
        host,
        &mr.merge_request.target_repo.name,
        &mr.merge_request.target_branch,
    ));

    let mut messages = Vec::new();
    let mut errors = Vec::new();

    if mr.was_merged {
        handle_merged_merge_request(project, branch, mr, &mut messages, &mut errors);
    }

    if !mr.is_open {
        if let Some(test_action) = branch.test() {
            handle_test_on_update(data, test_action, mr, None);
        }

        if let Some(stage_action) = branch.stage() {
            handle_stage_on_close(host, stage_action, mr);
        }

        return MergeRequestUpdateResult::closed(&mr.merge_request.source_branch);
    }

    // TODO: If the MR has been edited to change the target branch, we may need to remove it from
    // other branches, but we don't have the old branch name :/ .

    if !mr.is_source_branch_deleted() {
        let retry_desc = format!("fetch from {}", mr.merge_request.url);
        let fetch_res = retry_with_backoff(retry_desc, || {
            host.service.fetch_mr(&project.context, &mr.merge_request)
        });
        if let Err(err) = fetch_res {
            error!(
                target: "ghostflow-director/handler",
                "failed to fetch from the {} repository: {:?}",
                mr.merge_request.commit.repo.url, err,
            );

            errors.push(format!("Failed to fetch from the repository: {}.", err));
        }

        let reserve_res = project.context.reserve_ref(
            format!("mr/{}", mr.merge_request.id),
            &mr.merge_request.commit.id,
        );
        if let Err(err) = reserve_res {
            error!(
                target: "ghostflow-director/handler",
                "failed to reserve ref {} for merge request {}: {:?}",
                mr.merge_request.commit.id, mr.merge_request.url, err,
            );

            errors.push(format!(
                "Failed to reserve ref {} for the merge request: `{}`.",
                mr.merge_request.commit.id, err,
            ));
        }
    }

    if let Some(test_action) = branch.test() {
        handle_test_on_update(data, test_action, mr, Some(&project.context));
    }

    let mut mr = mr.clone();

    let is_ok = if mr.is_source_branch_deleted() {
        // A merge request with a deleted source branch is never ok.
        false
    } else {
        if let Some(wait) = branch.wait_for_pipeline.as_ref() {
            let mut stall = Duration::from_secs_f64(wait.initial_time);
            let mut count = 0;

            while mr.merge_request.commit.last_pipeline.is_none() && count < wait.max_waits {
                warn!(
                    target: "ghostflow-director/handler",
                    "waiting for a pipeline to be created for {}: {:?}",
                    mr.merge_request.url, stall,
                );

                thread::sleep(stall);
                count += 1;

                let mr_refresh = host
                    .service
                    .merge_request(&mr.merge_request.target_repo.name, mr.merge_request.id);
                if let Ok(mr_refresh) = mr_refresh {
                    if let Some(pipeline) = mr_refresh.commit.last_pipeline {
                        info!(
                            target: "ghostflow-director/handler",
                            "found a pipeline for {}: {}",
                            mr.merge_request.url, pipeline,
                        );
                    }

                    mr.merge_request.to_mut().commit = mr_refresh.commit;
                }

                stall = stall.mul_f64(wait.ratio);
            }
        }

        match mr.check_status(
            host.service.clone().as_hosting_service().as_ref(),
            &project.context,
            |name| project.status_name(name),
        ) {
            Ok(status) => {
                if status.is_checked() {
                    status.is_ok()
                } else {
                    match check_mr(project, &mr) {
                        Ok(b) => b,
                        Err(err) => {
                            error!(
                                target: "ghostflow-director/handler",
                                "failed to run checks for merge request {}: {:?}",
                                mr.merge_request.url, err,
                            );

                            errors.push(format!("Failed to run the checks: `{}`.", err));

                            false
                        },
                    }
                }
            },
            Err(err) => {
                error!(
                    target: "ghostflow-director/handler",
                    "failed to determine the check status for merge request {}: {:?}",
                    mr.merge_request.url, err,
                );

                errors.push(format!("Failed to determine the check status: `{}`.", err));

                false
            },
        }
    };
    let is_wip = mr.merge_request.work_in_progress;

    if let Some(stage_action) = branch.stage() {
        handle_stage_on_update(is_ok, is_wip, stage_action, &mr, &mut errors);
    }

    if !mr.is_source_branch_deleted() {
        project.mr_dashboards().iter().for_each(|dashboard| {
            handle_dashboard_on_update(dashboard, &mr, project, &mut errors);
        })
    }

    utils::handle_result(&messages, &errors, false, |comment| {
        host.service.post_mr_comment(&mr.merge_request, comment)
    })
}

fn handle_merged_merge_request(
    project: &Project,
    branch: &Branch,
    mr: &Info,
    messages: &mut Vec<String>,
    errors: &mut Vec<String>,
) {
    if mr.merge_request.remove_source_branch {
        remove_source_branch(&project.context, &mr.merge_request, messages);
    }

    if !branch.issue_labels.is_empty() {
        label_closed_issues(
            project.service.clone().as_hosting_service().as_ref(),
            &branch.issue_labels,
            &branch.stale_issue_labels,
            &mr.merge_request,
            errors,
        );
    }
}

fn remove_source_branch(ctx: &GitContext, mr: &MergeRequest, messages: &mut Vec<String>) {
    let url = if let Some(ref repo) = mr.source_repo {
        info!(
            target: "ghostflow-director/handler",
            "removing the source branch for {}",
            mr.url,
        );
        &repo.url
    } else {
        info!(
            target: "ghostflow-director/handler",
            "not removing the source branch for {} because the source repo is not available",
            mr.url,
        );
        return;
    };

    let push_res = ctx
        .git()
        .arg("push")
        .arg("--atomic")
        .arg("--porcelain")
        .arg(format!(
            "--force-with-lease=refs/heads/{}:{}",
            mr.source_branch, mr.commit.id,
        ))
        .arg(url)
        .arg(format!(":refs/heads/{}", mr.source_branch))
        .output();
    match push_res {
        Ok(push) => {
            if !push.status.success() {
                warn!(
                    target: "ghostflow-director/handler",
                    "failed to remove the source branch of {} from the remote server: {}",
                    mr.url, String::from_utf8_lossy(&push.stderr),
                );

                messages.push(
                    "Failed to delete the source branch as requested. Access may need to be \
                     granted in order to allow for branch deletion."
                        .into(),
                );
            }
        },
        Err(err) => {
            error!(
                target: "ghostflow-director/handler",
                "failed to construct push command: {:?}",
                err,
            );
        },
    }
}

fn label_closed_issues(
    service: &dyn HostingService,
    labels: &[String],
    stale_labels: &[String],
    mr: &MergeRequest,
    errors: &mut Vec<String>,
) {
    info!(
        target: "ghostflow-director/handler",
        "labeling issues closed by {}",
        mr.url,
    );

    let closed_issues = service.issues_closed_by_mr(mr).unwrap_or_else(|err| {
        error!(
            target: "ghostflow-director/handler",
            "failed to fetch issues closed by MR {}: {:?}",
            mr.url, err,
        );

        errors.push("Failed to fetch the issues that this merge request closes.".into());

        Vec::new()
    });

    let labels = labels.iter().map(AsRef::as_ref).collect::<Vec<_>>();
    let stale_labels = stale_labels.iter().map(AsRef::as_ref).collect::<Vec<_>>();

    let failed_issues = closed_issues
        .into_iter()
        .filter_map(|issue| {
            let add_has_error = if let Err(err) = service.add_issue_labels(&issue, &labels) {
                error!(
                    target: "ghostflow-director/handler",
                    "failed to add labels to issue {}: {:?}",
                    issue.url, err,
                );

                Some(())
            } else {
                None
            };
            let remove_has_error =
                if let Err(err) = service.remove_issue_labels(&issue, &stale_labels) {
                    error!(
                        target: "ghostflow-director/handler",
                        "failed to remove labels to issue {}: {:?}",
                        issue.url, err,
                    );

                    Some(())
                } else {
                    None
                };

            add_has_error.or(remove_has_error).map(|_| issue.reference)
        })
        .join(", ");

    if !failed_issues.is_empty() {
        errors.push(format!(
            "Failed to label issues {} with `{}`.",
            failed_issues,
            labels.iter().format("`, `"),
        ));
    }
}

/// Handle the test action when a merge request is updated.
fn handle_test_on_update(
    data: &Value,
    action: &TestAction,
    mr: &Info,
    context: Option<&GitContext>,
) {
    match *action.test() {
        TestBackend::Jobs {
            action: ref test_jobs,
            ..
        } => {
            let backport_data = if let Some(ctx) = context {
                mr.backports_json(ctx)
            } else {
                json!({})
            };
            let mr_data = json!({
                "backports": backport_data,
                "merge_request": data.clone(),
            });
            let job_data = utils::test_job("mr_update", &mr_data);
            if let Err(err) = test_jobs.test_update(job_data) {
                error!(
                    target: "ghostflow-director/handler",
                    "failed to drop a job file for an update to {}: {:?}",
                    mr.merge_request.url, err,
                );
            }
        },
        TestBackend::Refs(ref test_refs) => {
            if !mr.is_open {
                // TODO: Handle untesting backport branches.

                if let Err(err) = test_refs.untest_mr(&mr.merge_request) {
                    error!(
                        target: "ghostflow-director/handler",
                        "failed to remove the test ref for an update to {}: {:?}",
                        mr.merge_request.url, err,
                    );
                }
            }

            // TODO: Anything else here? Forcefully untest the MR?
        },
        // Nothing to do; the service handles triggering updates on its own.
        TestBackend::Pipelines {
            ..
        } => (),
    }
}

/// Handle removal of a merge request from the stage when it closes.
fn handle_stage_on_close(host: &Host, action: &StageAction, mr: &Info) {
    let res = action
        .stage()
        .unstage_update_merge_request(&mr.merge_request, "because it has been closed");

    if let Err(err) = res {
        error!(
            target: "ghostflow-director/handler",
            "failed to unstage a closed merge request {}: {:?}",
            mr.merge_request.url, err,
        );

        let errors = [format!(
            "Error occurred when unstaging a closed merge request (@{}): {}.",
            host.maintainers.join(" @"),
            err,
        )];
        utils::handle_result(&[], &errors, false, |comment| {
            host.service.post_mr_comment(&mr.merge_request, comment)
        });
    }
}

/// Handle the stage policy when a merge request is updated.
fn handle_stage_on_update(
    is_ok: bool,
    is_wip: bool,
    action: &StageAction,
    mr: &Info,
    errors: &mut Vec<String>,
) {
    let mut stage = action.stage();

    // TODO: Handle staging policies for backported branches.

    let (policy, reason) = if !is_ok {
        (StageUpdatePolicy::Unstage, "since it is failing its checks")
    } else if is_wip {
        (
            StageUpdatePolicy::Unstage,
            "since it is marked as work-in-progress",
        )
    } else if let Some(staged_topic) = stage.stager().find_topic_by_id(mr.merge_request.id) {
        if staged_topic.topic.commit == mr.merge_request.commit.id {
            (
                StageUpdatePolicy::Ignore,
                "it is already on the stage as-is",
            )
        } else {
            (action.policy, "as per policy")
        }
    } else {
        (
            StageUpdatePolicy::Ignore,
            "it is not on the stage currently",
        )
    };

    let (what, res) = match policy {
        StageUpdatePolicy::Ignore => ("ignore", Ok(())),
        StageUpdatePolicy::Restage => {
            (
                "restage",
                stage.stage_merge_request_named(
                    &mr.merge_request,
                    mr.topic_name(),
                    &mr.merge_request.author.identity(),
                    mr.date,
                ),
            )
        },
        StageUpdatePolicy::Unstage => {
            (
                "unstage",
                stage.unstage_update_merge_request(&mr.merge_request, reason),
            )
        },
    };

    if let Err(err) = res {
        error!(
            target: "ghostflow-director/handler",
            "failed to {} merge request {}: {:?}",
            what, mr.merge_request.url, err,
        );

        errors.push(format!("Failed to {}: `{}`.", what, err));
    }
}

/// Handle the dashboard status when a merge request is updated.
fn handle_dashboard_on_update(
    action: &Dashboard,
    mr: &Info,
    project: &Project,
    errors: &mut Vec<String>,
) {
    mr.backports().into_iter().for_each(|backport| {
        if !project.branches.contains_key(backport.branch()) {
            // No need to add an error; already handled by the `check` action.
            return;
        };

        // Craft the information necessary to mock up this MR for its backport status.
        let backport_commit_id =
            if let Ok(commit) = backport.commit(&mr.merge_request, &project.context) {
                commit
            } else {
                // No need to add an error; already handled by the `check` action.
                return;
            };
        let masking_commit = if backport_commit_id == mr.merge_request.commit.id {
            mr.merge_request.commit.clone()
        } else {
            let mut altered_commit = mr.merge_request.commit.clone();
            altered_commit.id = backport_commit_id;
            // We don't know that there is a refname for this commit (it isn't used anyways;
            // the `refname` comes from the merge request information here).
            altered_commit.refname = None;
            // We don't know that there is a pipeline for this commit (yet). It doesn't matter
            // much in this context (no hosting service knows how to trigger a pipeline for a
            // backported subsection of a merge request).
            //
            // TODO: This would likely be the place to *make* a pipeline for this commit,
            // however.
            altered_commit.last_pipeline = None;

            altered_commit
        };

        let mut altered_mr = mr.merge_request.clone();
        altered_mr.to_mut().target_branch = backport.branch().into();

        if let Err(err) = action.post_for_mr_altered(&altered_mr, &masking_commit) {
            error!(
                target: "ghostflow-director/handler",
                "failed to post dashboard commit status for merge request {}: {:?}",
                altered_mr.url, err,
            );

            errors.push(format!(
                "Failed to post commit status for {}: `{}`.",
                backport.branch(),
                err,
            ));
        }
    })
}

/// Check a merge request against the target branch's checks.
pub fn check_mr(project: &Project, mr: &Info) -> Result<bool, CheckError> {
    let mut errors = Vec::new();

    let result = mr
        .backports()
        .into_iter()
        .map(|backport| {
            let branch = if let Some(branch) = project.branches.get(backport.branch()) {
                branch
            } else {
                errors.push(format!(
                    "the `{}` target branch is not watched",
                    backport.branch(),
                ));
                return Ok(CheckStatus::Fail);
            };
            let commit = match backport.commit(&mr.merge_request, &project.context) {
                Ok(commit) => commit,
                Err(err) => {
                    error!(
                        target: "ghostflow-director/handler",
                        "failed to determine the commit to backport into {}: {:?}",
                        backport.branch(), err,
                    );

                    errors.push(format!(
                        "failed to determine the commit to backport into {}: {}",
                        backport.branch(),
                        err,
                    ));
                    return Ok(CheckStatus::Fail);
                },
            };
            project.check_mr_with(branch, &mr.merge_request, &commit)
        })
        // Collect all of the results.
        .collect::<Result<Vec<_>, CheckError>>()
        // Ensure that all of them passed.
        .map(|results| {
            results
                .into_iter()
                .all(|status| status == CheckStatus::Pass)
        });

    if !errors.is_empty() {
        let comment = format!(
            "The backport configuration is not valid:\n\
             \n  - {}",
            errors.into_iter().join("\n  - "),
        );
        if let Err(err) = project.service.post_mr_comment(&mr.merge_request, &comment) {
            error!(
                target: "ghostflow-director/handler",
                "failed to post backport configuration failure comment: {:?}",
                err,
            );
        }
    }

    result
}

#[cfg(test)]
mod tests {
    use git_workarea::CommitId;
    use serde_json::{json, Value};

    use crate::handlers::test::*;

    const MERGE_REQUEST_TOPIC: &str = "6f781d4d4d85dfd5a609532a26bcc6fd63fcef51";
    const BRANCH_NEXT: &str = "27485b5013bf65f5bccc17de921f67c069d43423";

    fn test_simple_config() -> Value {
        json!({
            "ghostflow/example": {
                "branches": {
                    "master": {},
                },
            },
        })
    }

    #[test]
    fn test_merge_request_repush_recheck() {
        let mut service = TestService::new(
            "test_merge_request_repush_recheck",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::push("fork", "example", "mr-source", BRANCH_NEXT),
                // Repush the original value.
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
            ],
        )
        .unwrap();
        let config = test_simple_config();
        let end = EndSignal::CommitStatusRepeat {
            count: 2.into(),
            commit: CommitId::new(MERGE_REQUEST_TOPIC),
            name: "ghostflow-check-master".into(),
            state: CommitStatusState::Success,
            description: Some(
                "overall branch status for the content checks against master\n\
                 \n\
                 Branch-at: 6f781d4d4d85dfd5a609532a26bcc6fd63fcef51"
                    .into(),
            ),
            target_url: None,
        };

        service.launch(config, end).unwrap();
    }

    fn test_dashboard_config() -> Value {
        json!({
            "ghostflow/example": {
                "merge_request_dashboards": [
                    {
                        "status_name": "dashboard-for-{target_branch}",
                        "description": "Dashboard from {source_branch} for {target_branch}",
                        "url": "https://example.com/{commit}",
                    },
                    {
                        "status_name": "other-dashboard-for-{target_branch}",
                        "description": "Other dashboard from {source_branch} for {target_branch}",
                        "url": "https://example.org/{commit}",
                    },
                ],
                "branches": {
                    "master": {},
                    "next": {},
                },
            },
        })
    }

    #[test]
    fn test_merge_request_dashboard_status_simple() {
        let mut service = TestService::new(
            "test_merge_request_dashboard_status_simple",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
            ],
        )
        .unwrap();
        let config = test_dashboard_config();
        let end = EndSignal::CommitStatus {
            commit: CommitId::new(MERGE_REQUEST_TOPIC),
            name: "dashboard-for-master".into(),
            state: CommitStatusState::Success,
            description: Some("Dashboard from mr-source for master".into()),
            target_url: Some(format!("https://example.com/{}", MERGE_REQUEST_TOPIC)),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_merge_request_dashboard_status_simple_all() {
        let mut service = TestService::new(
            "test_merge_request_dashboard_status_simple_all",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
            ],
        )
        .unwrap();
        let config = test_dashboard_config();
        let end = EndSignal::CommitStatus {
            commit: CommitId::new(MERGE_REQUEST_TOPIC),
            name: "other-dashboard-for-master".into(),
            state: CommitStatusState::Success,
            description: Some("Other dashboard from mr-source for master".into()),
            target_url: Some(format!("https://example.org/{}", MERGE_REQUEST_TOPIC)),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_merge_request_dashboard_status_backport() {
        let mut service = TestService::new(
            "test_merge_request_dashboard_status_backport",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "Backport: next", false),
            ],
        )
        .unwrap();
        let config = test_dashboard_config();
        let end = EndSignal::CommitStatus {
            commit: CommitId::new(MERGE_REQUEST_TOPIC),
            name: "dashboard-for-next".into(),
            state: CommitStatusState::Success,
            description: Some("Dashboard from mr-source for next".into()),
            target_url: Some(format!("https://example.com/{}", MERGE_REQUEST_TOPIC)),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_merge_request_dashboard_status_backport_all() {
        let mut service = TestService::new(
            "test_merge_request_dashboard_status_backport_all",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "Backport: next", false),
            ],
        )
        .unwrap();
        let config = test_dashboard_config();
        let end = EndSignal::CommitStatus {
            commit: CommitId::new(MERGE_REQUEST_TOPIC),
            name: "other-dashboard-for-next".into(),
            state: CommitStatusState::Success,
            description: Some("Other dashboard from mr-source for next".into()),
            target_url: Some(format!("https://example.org/{}", MERGE_REQUEST_TOPIC)),
        };

        service.launch(config, end).unwrap();
    }

    fn test_dashboard_wait_config() -> Value {
        json!({
            "ghostflow/example": {
                "merge_request_dashboards": [
                    {
                        "status_name": "dashboard-for-{target_branch}",
                        "description": "Dashboard from {source_branch} for {target_branch}",
                        "url": "https://example.com/{pipeline_id}",
                    },
                ],
                "branches": {
                    "master": {
                        "wait_for_pipeline": {
                            "initial_time": 1.0,
                            "ratio": 1.1,
                            "max_waits": 2,
                        },
                    },
                },
            },
        })
    }

    #[test]
    fn test_merge_request_dashboard_wait_for_pipeline() {
        let mut service = TestService::new(
            "test_merge_request_dashboard_wait_for_pipeline",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::delay(1500),
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
            ],
        )
        .unwrap();
        let config = test_dashboard_wait_config();
        let end = EndSignal::CommitStatus {
            commit: CommitId::new(MERGE_REQUEST_TOPIC),
            name: "dashboard-for-master".into(),
            state: CommitStatusState::Success,
            description: Some("Dashboard from mr-source for master".into()),
            target_url: Some("https://example.com/0".into()),
        };

        service.launch(config, end).unwrap();
    }
}
