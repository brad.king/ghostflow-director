// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use chrono::{DateTime, Utc};
use ghostflow_gitlab::gitlab::hooktypes::HookDate;
use serde::Deserialize;

/// Actions which may occur on a merge request.
#[derive(Deserialize, Debug, Clone, Copy)]
pub enum MergeRequestAction {
    /// The merge request was updated.
    #[serde(rename = "update")]
    Update,
    /// The merge request was opened.
    #[serde(rename = "open")]
    Open,
    /// The merge request was closed.
    #[serde(rename = "close")]
    Close,
    /// The merge request was reopened.
    #[serde(rename = "reopen")]
    Reopen,
    /// The merge request was approved.
    #[serde(rename = "approved")]
    Approved,
    /// A merge request approval was revoked.
    #[serde(rename = "unapproved")]
    Unapproved,
    /// A merge request approval has been added
    #[serde(rename = "approval")]
    Approval,
    /// A merge request approval has been removed
    #[serde(rename = "unapproval")]
    Unapproval,
    /// The merge request was merged.
    #[serde(rename = "merge")]
    Merge,
}

impl MergeRequestAction {
    /// Whether the action moved the MR into an "open" state or not.
    pub fn is_open_action(self) -> bool {
        matches!(self, Self::Open | Self::Reopen)
    }

    /// Whether the action merged the MR or not.
    pub fn is_merge_action(self) -> bool {
        matches!(self, Self::Merge)
    }
}

/// The states a merge request may be in.
#[derive(Deserialize, Debug, Clone, Copy)]
pub enum MergeRequestState {
    /// The merge request is open.
    #[serde(rename = "opened")]
    Opened,
    /// The merge request has been closed before merging.
    #[serde(rename = "closed")]
    Closed,
    /// The merge request has been opened after closing.
    #[serde(rename = "reopened")]
    Reopened,
    /// The merge request has been merged.
    #[serde(rename = "merged")]
    Merged,
    /// The merge request is locked from further discussion or updates.
    #[serde(rename = "locked")]
    Locked,
}

/// Project information exposed in hooks.
#[derive(Deserialize, Debug)]
pub struct ProjectHookAttrs {
    /// The path to the project's repository with its namespace.
    pub path_with_namespace: String,
}

/// Merge request information exposed in hooks.
#[derive(Deserialize, Debug)]
pub struct MergeRequestHookAttrs {
    /// The source project of the merge request.
    ///
    /// If this is `None`, the source repository has been deleted.
    pub source: Option<ProjectHookAttrs>,
    /// The target project of the merge request.
    pub target: ProjectHookAttrs,

    /// The source branch of the merge request.
    pub source_branch: String,
    /// When the merge request was last updated.
    pub updated_at: HookDate,
    /// The previous revision of the source branch.
    pub oldrev: Option<String>,
    /// The state of the merge request.
    pub state: MergeRequestState,
    /// The user-visible ID of the merge request.
    pub iid: u64,
    /// The type of action which caused the hook.
    pub action: Option<MergeRequestAction>,
}

/// User information exposed in hooks.
#[derive(Deserialize, Debug)]
pub struct UserHookAttrs {
    /// The handle of the user.
    pub username: String,
}

/// A merge request hook.
#[derive(Deserialize, Debug)]
pub struct MergeRequestHook {
    /// Attributes of the merge request.
    pub object_attributes: MergeRequestHookAttrs,
}

/// The entities a note may be added to.
#[derive(Deserialize, Debug, Clone, Copy)]
pub enum NoteType {
    /// A note on a commit.
    Commit,
    /// A note on an issue.
    Issue,
    /// A note on a merge request.
    MergeRequest,
    /// A note on a snippet.
    Snippet,
}

/// Note (comment) information exposed in hooks.
#[derive(Deserialize, Debug)]
pub struct NoteHookAttrs {
    /// The ID of the note.
    pub id: u64,
    /// The content of the note.
    pub note: String,
    /// The type of entity the note is attached to.
    pub noteable_type: NoteType,
    /// When the note was created.
    pub created_at: HookDate,
    /// Whether the note was created by a user or in response to an external action.
    pub system: bool,
}

/// A note hook.
#[derive(Deserialize, Debug)]
pub struct NoteHook {
    /// The user who triggered the hook.
    pub user: UserHookAttrs,
    /// The attributes on the note itself.
    pub object_attributes: NoteHookAttrs,
    /// The merge request the note is associated with (for merge request notes).
    pub merge_request: Option<MergeRequestHookAttrs>,
}

/// Commit information exposed in hooks.
#[derive(Deserialize, Debug)]
pub struct CommitHookAttrs {
    pub timestamp: DateTime<Utc>,
}

/// A push hook.
#[derive(Deserialize, Debug)]
pub struct PushHook {
    /// The new object ID of the ref after the push.
    pub after: String,
    #[serde(rename = "ref")]
    /// The name of the reference which has been pushed.
    pub ref_: String,
    /// The username of the user who pushed.
    pub user_username: String,
    /// Attributes of the project.
    pub project: ProjectHookAttrs,
    /// The commits pushed to the repository.
    ///
    /// Limited to 20 commits.
    pub commits: Vec<CommitHookAttrs>,
}

/// A project membership hook.
#[derive(Deserialize, Debug)]
pub struct ProjectMemberSystemHook {
    /// The namespace and path of the project (used for URLs).
    pub project_path_with_namespace: String,
}

/// A group membership hook.
#[derive(Deserialize, Debug)]
pub struct GroupMemberSystemHook {
    /// The name of the group.
    pub group_name: String,
}

/// A hook for a project.
#[derive(Deserialize, Debug)]
pub struct ProjectSystemHook {
    /// The namespace and path of the project.
    pub path_with_namespace: String,
}
