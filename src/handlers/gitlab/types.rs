// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use chrono::NaiveDate;
use serde::Deserialize;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum NamespaceKind {
    #[serde(rename = "user")]
    User,
    #[serde(rename = "group")]
    Group,
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize)]
pub struct Namespace {
    pub id: u64,
    pub kind: NamespaceKind,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum UserState {
    /// The user is active and may perform actions.
    #[serde(rename = "active")]
    Active,
    /// Blocked from logging in.
    #[serde(rename = "blocked")]
    Blocked,
    /// Blocked from logging in via LDAP.
    #[serde(rename = "ldap_blocked")]
    LdapBlocked,
    /// The user is deactivated until the next login.
    #[serde(rename = "deactivated")]
    Deactivated,
}

impl UserState {
    pub fn is_blocked(self) -> bool {
        matches!(self, Self::Blocked | Self::LdapBlocked)
    }
}

#[derive(Debug, Deserialize, Clone)]
pub struct Member {
    pub id: u64,
    pub username: String,
    pub access_level: u64,
    pub expires_at: Option<NaiveDate>,
    pub state: UserState,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Project {
    pub id: u64,
    pub namespace: Namespace,
}

#[derive(Debug, Clone, Copy, Deserialize)]
pub struct Group {
    pub parent_id: Option<u64>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Author {
    pub username: String,
}

#[derive(Debug, Clone, Deserialize)]
pub struct AwardEmoji {
    pub name: String,
    pub user: Author,
}

#[derive(Debug, Clone, Deserialize)]
pub struct User {
    pub id: u64,
    pub username: String,
}
