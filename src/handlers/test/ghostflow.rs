// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::sync::{self, Arc, Mutex, RwLock};

use ghostflow::host::*;
use ghostflow::utils::TrailerRef;
use git_workarea::{CommitId, GitContext, GitError};
use thiserror::Error;
use tokio::sync::mpsc::{self, Sender};

use crate::ghostflow_ext::{AccessLevel, DirectorHostingService, Membership};
use crate::handlers::test::data;
use crate::handlers::test::service::{HostEvent, ServiceEvent};

#[derive(Debug, Error)]
enum HostError {
    #[error("no such merge request {} on {}", id, project)]
    NoMergeRequest { project: String, id: u64 },
    #[error("failed to name revision {}: {}", commit, output)]
    NameRev { commit: CommitId, output: String },
    #[error("failed to obtain a lock on the hosting data: {}", reason)]
    DataLock { reason: String },
    #[error("simultaneous access to the communication layer: {}", reason)]
    CommLock { reason: String },
    #[error("failed to write to server channel: {}", source)]
    WriteChannel {
        #[from]
        source: mpsc::error::SendError<ServiceEvent>,
    },
    #[error("job `{}` is archived and may not be started", name)]
    ArchivedJob { name: String },
}

impl HostError {
    fn no_merge_request(project: String, id: u64) -> Self {
        HostError::NoMergeRequest {
            project,
            id,
        }
    }

    fn name_rev(commit: CommitId, output: &[u8]) -> Self {
        HostError::NameRev {
            commit,
            output: String::from_utf8_lossy(output).into(),
        }
    }

    fn data_lock<T>(source: sync::PoisonError<T>) -> Self {
        HostError::DataLock {
            reason: source.to_string(),
        }
    }

    fn comm_lock<T>(source: sync::PoisonError<T>) -> Self {
        HostError::CommLock {
            reason: source.to_string(),
        }
    }

    fn archived_job(name: String) -> Self {
        Self::ArchivedJob {
            name,
        }
    }
}

impl From<HostError> for HostingServiceError {
    fn from(host: HostError) -> Self {
        HostingServiceError::host(host)
    }
}

mod comm {
    use std::sync::{Arc, RwLock, RwLockReadGuard};
    use tokio::sync::mpsc::Sender;

    use ghostflow::host::HostingServiceError;

    use crate::handlers::test::data;
    use crate::handlers::test::service::{HostEvent, ServiceEvent};

    use super::HostError;

    pub struct Comm {
        data: Arc<RwLock<data::Data>>,
        writer: Sender<ServiceEvent>,
    }

    impl Comm {
        pub fn new(data: Arc<RwLock<data::Data>>, writer: Sender<ServiceEvent>) -> Self {
            Self {
                data,
                writer,
            }
        }

        pub(super) fn data(&mut self) -> Result<RwLockReadGuard<data::Data>, HostError> {
            self.data.read().map_err(HostError::data_lock)
        }

        /// Send a host event to the host implementation.
        pub fn update(&mut self, data: HostEvent) -> Result<(), HostingServiceError> {
            Ok(self.send(ServiceEvent::Host(data))?)
        }

        fn send(&mut self, event: ServiceEvent) -> Result<(), HostError> {
            let sender = self.writer.clone();
            sender.blocking_send(event)?;

            Ok(())
        }
    }
}

pub struct TestHost {
    user: User,
    // A `Mutex` is used here to get `&mut` access to the comm from `&self` methods. A `RefCell`
    // would be preferrable, except that `HostingService` requires `Sync`.
    comm: Mutex<comm::Comm>,
    pipeline_support: bool,
}

impl TestHost {
    pub fn new(data: Arc<RwLock<data::Data>>, writer: Sender<ServiceEvent>) -> Self {
        let user = {
            let data = data.read().unwrap();
            data.user_by_id(0)
                .expect("a user is required for hosting services")
                .clone()
        };
        let comm = comm::Comm::new(data, writer);

        TestHost {
            user: User {
                name: user.name.clone(),
                handle: user.handle.clone(),
                email: user.email,
            },
            comm: Mutex::new(comm),
            pipeline_support: false,
        }
    }

    fn comm(&self) -> Result<sync::MutexGuard<comm::Comm>, HostError> {
        self.comm.lock().map_err(HostError::comm_lock)
    }

    pub fn with_pipelines(&mut self, with_pipelines: bool) -> &mut Self {
        self.pipeline_support = with_pipelines;
        self
    }
}

impl HostingService for TestHost {
    fn as_pipeline_service(self: Arc<Self>) -> Option<Arc<dyn HostedPipelineService>> {
        if self.pipeline_support {
            Some(self as Arc<dyn HostedPipelineService>)
        } else {
            None
        }
    }

    fn service_user(&self) -> &User {
        &self.user
    }

    fn user(&self, project: &str, user: &str) -> Result<User, HostingServiceError> {
        let mut comm = self.comm()?;
        let data = comm.data()?;

        let _ = data.project(project).map_err(HostingServiceError::host)?;
        Ok(ghostflow_user(
            data.user(user).map_err(HostingServiceError::host)?,
        ))
    }

    fn commit(&self, project: &str, commit: &CommitId) -> Result<Commit, HostingServiceError> {
        let mut comm = self.comm()?;
        let data = comm.data()?;

        let project = data.project(project).map_err(HostingServiceError::host)?;
        ghostflow_commit(commit, project, &data)
    }

    fn merge_request(&self, project: &str, id: u64) -> Result<MergeRequest, HostingServiceError> {
        let mut comm = self.comm()?;
        let data = comm.data()?;

        let mr = data
            .mr(id)
            .map_err(HostingServiceError::host)
            .and_then(|mr| ghostflow_mr(mr, &data))?;

        if mr.target_repo.name == project {
            Ok(mr)
        } else {
            Err(HostError::no_merge_request(project.into(), id).into())
        }
    }

    fn repo(&self, project: &str) -> Result<Repo, HostingServiceError> {
        let mut comm = self.comm()?;
        let data = comm.data()?;

        data.project(project)
            .map_err(HostingServiceError::host)
            .and_then(|project| ghostflow_repo(project, &data))
    }

    fn get_mr_comments(&self, mr: &MergeRequest) -> Result<Vec<Comment>, HostingServiceError> {
        let mut comm = self.comm()?;
        let data = comm.data()?;

        data.mr_comments(mr.id)
            .map_err(HostingServiceError::host)
            .and_then(|comments| {
                comments
                    .into_iter()
                    .map(|comment| ghostflow_comment(comment, &data))
                    .collect()
            })
    }

    fn post_mr_comment(&self, mr: &MergeRequest, content: &str) -> Result<(), HostingServiceError> {
        self.comm()?.update(HostEvent::MergeRequestComment {
            mr: mr.id,
            content: content.into(),
        })
    }

    fn get_commit_statuses(
        &self,
        commit: &Commit,
    ) -> Result<Vec<CommitStatus>, HostingServiceError> {
        let mut comm = self.comm()?;
        let data = comm.data()?;
        let project = data
            .project(&commit.repo.name)
            .map_err(HostingServiceError::host)?;

        data.statuses(project.id, &commit.id)
            .map_err(HostingServiceError::host)
            .and_then(|statuses| {
                statuses
                    .into_iter()
                    .map(|status| ghostflow_commit_status(status, &data))
                    .collect()
            })
    }

    fn post_commit_status(&self, status: PendingCommitStatus) -> Result<(), HostingServiceError> {
        let mut comm = self.comm()?;
        let project = {
            let data = comm.data()?;
            data.project(&status.commit.repo.name)
                .map_err(HostingServiceError::host)?
                .clone()
        };

        comm.update(HostEvent::CommitStatus {
            project: project.id,
            commit: status.commit.id.clone(),
            name: status.name.into(),
            description: status.description.into(),
            target_url: status.target_url.map(Into::into),
            state: status.state,
        })
    }

    fn get_mr_awards(&self, mr: &MergeRequest) -> Result<Vec<Award>, HostingServiceError> {
        let mut comm = self.comm()?;
        let data = comm.data()?;

        data.mr_awards(mr.id)
            .map_err(HostingServiceError::host)
            .and_then(|awards| {
                awards
                    .into_iter()
                    .map(|award| ghostflow_award(award, &data))
                    .collect()
            })
    }

    fn issues_closed_by_mr(&self, mr: &MergeRequest) -> Result<Vec<Issue>, HostingServiceError> {
        let mut comm = self.comm()?;
        let data = comm.data()?;

        let description = &data
            .mr(mr.id)
            .map_err(HostingServiceError::host)?
            .description;
        Ok(TrailerRef::extract(description)
            .into_iter()
            .filter_map(|token| {
                if token.token == "Closes" {
                    if let Ok(id) = token.value.parse::<u64>() {
                        data.issue(id)
                            .map_err(HostingServiceError::host)
                            .and_then(|issue| ghostflow_issue(issue, &data))
                            .ok()
                    } else {
                        None
                    }
                } else {
                    None
                }
            })
            .collect())
    }

    fn add_issue_labels(&self, issue: &Issue, labels: &[&str]) -> Result<(), HostingServiceError> {
        self.comm()?.update(HostEvent::AddIssueLabels {
            issue: issue.id,
            labels: labels.iter().map(|&l| l.into()).collect(),
        })
    }

    fn remove_issue_labels(
        &self,
        issue: &Issue,
        labels: &[&str],
    ) -> Result<(), HostingServiceError> {
        self.comm()?.update(HostEvent::RemoveIssueLabels {
            issue: issue.id,
            labels: labels.iter().map(|&l| l.into()).collect(),
        })
    }
}

impl DirectorHostingService for TestHost {
    fn as_hosting_service(self: Arc<Self>) -> Arc<dyn HostingService> {
        self as Arc<dyn HostingService>
    }

    fn members(&self, project: &str) -> Result<Vec<Membership>, HostingServiceError> {
        let mut comm = self.comm()?;
        let data = comm.data()?;

        data.project(project)
            .map_err(HostingServiceError::host)?
            .permissions
            .iter()
            .map(|(&uid, &access)| {
                Ok(Membership {
                    user: ghostflow_user(data.user_by_id(uid).map_err(HostingServiceError::host)?),
                    access_level: access,
                    expiration: None,
                })
            })
            .collect()
    }

    fn add_member(
        &self,
        project: &str,
        user: &User,
        access_level: AccessLevel,
    ) -> Result<(), HostingServiceError> {
        let mut comm = self.comm()?;
        let project_id = {
            let data = comm.data()?;
            data.project(project).map_err(HostingServiceError::host)?.id
        };

        comm.update(HostEvent::AddMember {
            project: project_id,
            user: user.handle.clone(),
            level: access_level,
        })
    }

    fn add_hook(&self, project: &str, url: &str) -> Result<(), HostingServiceError> {
        let mut comm = self.comm()?;
        let project_id = {
            let data = comm.data()?;
            data.project(project).map_err(HostingServiceError::host)?.id
        };

        comm.update(HostEvent::AddHook {
            project: project_id,
            url: url.into(),
        })
    }

    fn post_commit_comment(&self, _: &Commit, _: &str) -> Result<(), HostingServiceError> {
        // Commit comments are not used.
        Ok(())
    }

    fn get_mr_comment_awards(
        &self,
        _: &MergeRequest,
        comment: &Comment,
    ) -> Result<Vec<Award>, HostingServiceError> {
        let mut comm = self.comm()?;
        let data = comm.data()?;
        let id = comment.id.parse().map_err(HostingServiceError::host)?;

        data.comment_awards(id)
            .map_err(HostingServiceError::host)
            .and_then(|awards| {
                awards
                    .into_iter()
                    .map(|award| ghostflow_award(award, &data))
                    .collect()
            })
    }

    fn award_mr_comment(
        &self,
        mr: &MergeRequest,
        comment: &Comment,
        award: &str,
    ) -> Result<(), HostingServiceError> {
        let mut comm = self.comm()?;
        {
            let data = comm.data()?;
            data.mr(mr.id).map_err(HostingServiceError::host)?;
        }

        let id = comment.id.parse().map_err(HostingServiceError::host)?;

        comm.update(HostEvent::AwardComment {
            comment: id,
            award: award.into(),
        })
    }

    fn comment_award_name(&self) -> &str {
        "robot"
    }
}

impl HostedPipelineService for TestHost {
    fn pipelines_for_mr(
        &self,
        mr: &MergeRequest,
    ) -> Result<Option<Vec<Pipeline>>, HostingServiceError> {
        let mut comm = self.comm()?;
        let data = comm.data()?;

        let project = data
            .project(&mr.target_repo.name)
            .map_err(HostingServiceError::host)?;

        if !project.pipelines_enabled {
            return Ok(None);
        }

        let commit =
            ghostflow_commit(&mr.commit.id, project, &data).map_err(HostingServiceError::host)?;

        Ok(Some(
            data.pipelines_for_commit(project.id, &mr.commit.id)
                .into_iter()
                .map(|pipeline| ghostflow_pipeline(pipeline, commit.clone(), &data))
                .collect::<Result<Vec<_>, _>>()?,
        ))
    }

    fn pipeline_jobs(
        &self,
        pipeline: &Pipeline,
    ) -> Result<Option<Vec<PipelineJob>>, HostingServiceError> {
        let mut comm = self.comm()?;
        let data = comm.data()?;

        let project = data
            .project(&pipeline.commit.repo.name)
            .map_err(HostingServiceError::host)?;

        if !project.pipelines_enabled {
            return Ok(None);
        }

        let repo = ghostflow_repo(project, &data).map_err(HostingServiceError::host)?;

        Ok(Some(
            data.jobs_for_pipeline(pipeline.id)
                .into_iter()
                .map(|job| ghostflow_job(job, repo.clone(), &data))
                .collect::<Result<Vec<_>, _>>()?,
        ))
    }

    fn trigger_job(
        &self,
        job: &PipelineJob,
        user: Option<&str>,
    ) -> Result<(), HostingServiceError> {
        if job.archived {
            return Err(HostError::archived_job(job.name.clone()).into());
        }

        let mut comm = self.comm()?;
        {
            let data = comm.data()?;

            if let Some(&user) = user.as_ref() {
                data.user(user).map_err(HostingServiceError::host)?;
            }
        }

        comm.update(HostEvent::TriggerJob {
            project: job.repo.name.clone(),
            job: job.name.clone(),
            user: user.map(Into::into),
        })
    }
}

fn ghostflow_repo(project: &data::Project, data: &data::Data) -> Result<Repo, HostingServiceError> {
    let parent = if let Some(parent) = project.parent {
        Some(
            data.project_by_id(parent)
                .map_err(HostingServiceError::host)
                .and_then(|parent_project| ghostflow_repo(parent_project, data))
                .map(Box::new)?,
        )
    } else {
        None
    };

    Ok(Repo {
        name: project.name.clone(),
        url: project.url.clone(),
        http_url: project.url.clone(),
        forked_from: parent,
    })
}

fn ghostflow_user(user: &data::User) -> User {
    User {
        name: user.name.clone(),
        handle: user.handle.clone(),
        email: user.email.clone(),
    }
}

fn ghostflow_commit(
    commit: &CommitId,
    project: &data::Project,
    data: &data::Data,
) -> Result<Commit, HostingServiceError> {
    let ctx = GitContext::new(&project.url);
    let name_rev = ctx
        .git()
        .arg("name-rev")
        .arg(commit.as_str())
        .output()
        .map_err(|err| HostingServiceError::service(GitError::subcommand("name-rev", err)))?;
    if !name_rev.status.success() {
        return Err(HostError::name_rev(commit.clone(), &name_rev.stderr).into());
    }
    let rev = String::from_utf8_lossy(&name_rev.stdout);
    let refname = if rev.contains('~') || rev.contains('^') {
        None
    } else {
        Some(rev.trim().to_owned())
    };

    Ok(Commit {
        repo: ghostflow_repo(project, data)?,
        refname,
        id: commit.clone(),
        last_pipeline: data.last_pipeline_for_commit(project.id, commit),
    })
}

fn ghostflow_issue(issue: &data::Issue, data: &data::Data) -> Result<Issue, HostingServiceError> {
    let project = data
        .project_by_id(issue.project)
        .map_err(HostingServiceError::host)?;

    Ok(Issue {
        id: issue.id,
        repo: ghostflow_repo(project, data)?,
        url: issue.url.clone(),
        labels: issue.labels.clone(),
        reference: issue.reference.clone(),
    })
}

fn ghostflow_mr(
    mr: &data::MergeRequest,
    data: &data::Data,
) -> Result<MergeRequest, HostingServiceError> {
    let source_project = data
        .project_by_id(mr.source_project)
        .map_err(HostingServiceError::host)?;
    let target_project = data
        .project_by_id(mr.target_project)
        .map_err(HostingServiceError::host)?;

    Ok(MergeRequest {
        id: mr.id,
        source_repo: ghostflow_repo(source_project, data).ok(),
        source_branch: mr.source_branch.clone(),
        target_repo: ghostflow_repo(target_project, data)?,
        target_branch: mr.target_branch.clone(),
        url: mr.url.clone(),
        work_in_progress: mr.work_in_progress,
        description: mr.description.clone(),
        old_commit: if let Some(ref old_commit) = mr.old_commit {
            Some(ghostflow_commit(old_commit, target_project, data)?)
        } else {
            None
        },
        commit: ghostflow_commit(&mr.commit, target_project, data)?,
        author: ghostflow_user(
            data.user_by_id(mr.author)
                .map_err(HostingServiceError::host)?,
        ),
        reference: mr.reference.clone(),
        remove_source_branch: mr.remove_source_branch,
    })
}

fn ghostflow_comment(
    comment: &data::Comment,
    data: &data::Data,
) -> Result<Comment, HostingServiceError> {
    Ok(Comment {
        id: format!("{}", comment.id),
        is_system: comment.is_branch_update,
        is_branch_update: comment.is_branch_update,
        created_at: comment.created_at,
        author: ghostflow_user(
            data.user_by_id(comment.author)
                .map_err(HostingServiceError::host)?,
        ),
        content: comment.content.clone(),
    })
}

fn ghostflow_award(award: &data::Award, data: &data::Data) -> Result<Award, HostingServiceError> {
    Ok(Award {
        name: award.name.clone(),
        author: ghostflow_user(
            data.user_by_id(award.author)
                .map_err(HostingServiceError::host)?,
        ),
    })
}

fn ghostflow_commit_status(
    status: &data::CommitStatus,
    data: &data::Data,
) -> Result<CommitStatus, HostingServiceError> {
    Ok(CommitStatus {
        state: status.state,
        author: ghostflow_user(
            data.user_by_id(status.author)
                .map_err(HostingServiceError::host)?,
        ),
        refname: None,
        name: status.name.clone(),
        description: status.description.clone(),
        target_url: status.target_url.clone(),
    })
}

fn ghostflow_pipeline(
    pipeline: &data::Pipeline,
    commit: Commit,
    data: &data::Data,
) -> Result<Pipeline, HostingServiceError> {
    Ok(Pipeline {
        // Nothing cares about this state right now.
        state: PipelineState::InProgress,
        commit,
        id: pipeline.id,
        user: ghostflow_user(
            data.user_by_id(pipeline.owner)
                .map_err(HostingServiceError::host)?,
        ),
    })
}

fn ghostflow_job(
    job: &data::Job,
    repo: Repo,
    data: &data::Data,
) -> Result<PipelineJob, HostingServiceError> {
    Ok(PipelineJob {
        repo,
        state: job.state,
        stage: job.stage.clone(),
        name: job.name.clone(),
        id: job.id,
        archived: job.archived,
        user: ghostflow_user(
            data.user_by_id(job.owner)
                .map_err(HostingServiceError::host)?,
        ),
    })
}
