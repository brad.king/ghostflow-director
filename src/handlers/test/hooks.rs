// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Data structures for events sent from the hosting service to the director.

use std::borrow::Cow;
use std::ops::Deref;

use chrono::{DateTime, Utc};
use ghostflow::host;
use git_workarea::CommitId;
use serde::{Deserialize, Serialize};

use crate::actions::merge_requests::Info;
use crate::ghostflow_ext;
use crate::handlers::common::data::*;

/// A user mentioned in an event payload.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct User {
    /// The ID of the user.
    pub id: u64,
    /// The handle of the user.
    pub handle: String,
    /// The name of the user.
    pub name: String,
    /// The email address of the user.
    pub email: String,
}

impl From<User> for host::User {
    fn from(user: User) -> Self {
        host::User {
            handle: user.handle,
            name: user.name,
            email: user.email,
        }
    }
}

/// A note mentioned in an event payload.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Note {
    /// The ID of the note.
    pub id: u64,
    /// Whether the note is a branch update notification or not.
    pub is_branch_update: bool,
    /// When the note was created.
    pub created_at: DateTime<Utc>,
    /// The user that authored the note.
    pub author: User,
    /// The content of the note.
    pub content: String,
}

impl From<Note> for host::Comment {
    fn from(note: Note) -> Self {
        host::Comment {
            id: format!("{}", note.id),
            is_system: note.is_branch_update,
            is_branch_update: note.is_branch_update,
            created_at: note.created_at,
            author: note.author.into(),
            content: note.content,
        }
    }
}

/// The state of a merge request.
#[derive(Deserialize, Serialize, Debug, Clone, Copy, PartialEq, Eq)]
pub enum MergeRequestState {
    /// The MR is open.
    #[serde(rename = "open")]
    Open,
    /// The MR is closed.
    #[serde(rename = "closed")]
    Closed,
    /// The MR has been merged.
    #[serde(rename = "merged")]
    Merged,
}

/// A repository mentioned in an event payload.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Repo {
    /// The name of the repository.
    pub name: String,
    /// The URL To the repository.
    pub url: String,
    /// The ID of the repository.
    pub id: u64,
    /// The parent project of the repository.
    pub forked_from: Option<Box<Repo>>,
}

impl From<Repo> for host::Repo {
    fn from(repo: Repo) -> Self {
        host::Repo {
            name: repo.name,
            url: repo.url.clone(),
            http_url: repo.url,
            forked_from: repo
                .forked_from
                .map(|upstream| Box::new(upstream.deref().clone().into())),
        }
    }
}

/// A merge request mentioned in an event payload.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct MergeRequest {
    /// The source repository.
    pub source_repo: Repo,
    /// The branch in the source repoository to be merged.
    pub source_branch: String,
    /// The target repository.
    pub target_repo: Repo,
    /// The branch in the target repository to merge into.
    pub target_branch: String,
    /// The ID of the merge request.
    pub id: u64,
    /// The URL fo the merge request.
    pub url: String,
    /// Whether the merge request is a work-in-progress or not.
    pub work_in_progress: bool,
    /// The description of the merge request.
    pub description: String,
    /// The previous commit of the source branch (when updating the commit).
    pub old_commit: Option<Commit>,
    /// The current commit of the source branch.
    pub commit: Commit,
    /// The author of the merge request.
    pub author: User,
    /// How to refer to the merge request in comments.
    pub reference: String,
    /// Whether to remove the source branch or not when merging.
    pub remove_source_branch: bool,
}

impl From<MergeRequest> for host::MergeRequest {
    fn from(mr: MergeRequest) -> Self {
        host::MergeRequest {
            source_repo: Some(mr.source_repo.into()),
            source_branch: mr.source_branch,
            target_repo: mr.target_repo.into(),
            target_branch: mr.target_branch,
            id: mr.id,
            url: mr.url,
            work_in_progress: mr.work_in_progress,
            description: mr.description,
            old_commit: mr.old_commit.map(Into::into),
            commit: mr.commit.into(),
            author: mr.author.into(),
            reference: mr.reference,
            remove_source_branch: mr.remove_source_branch,
        }
    }
}

/// The action performed on a merge request.
#[derive(Deserialize, Serialize, Debug, Clone, Copy, PartialEq, Eq)]
pub enum MergeRequestAction {
    /// A comment has been added to the merge request.
    #[serde(rename = "comment")]
    Comment,
    /// The source branch has been changed.
    #[serde(rename = "update")]
    Update,
    /// The merge request has been opened.
    #[serde(rename = "open")]
    Open,
    /// The merge request has been closed.
    #[serde(rename = "close")]
    Close,
    /// The merge request has been merged.
    #[serde(rename = "merge")]
    Merge,
}

/// Notification about a merge request.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct MergeRequestHook {
    /// The state of the merge request.
    pub state: MergeRequestState,
    /// The action that triggered the event hook.
    pub action: MergeRequestAction,
    /// When the merge request was updated.
    pub updated_at: DateTime<Utc>,
    /// The merge request that was acted upon.
    pub merge_request: MergeRequest,
    /// The note that updated the merge request (if relevant).
    pub note: Option<Note>,
}

impl From<MergeRequestHook> for Info<'static> {
    fn from(hook: MergeRequestHook) -> Self {
        Info {
            merge_request: Cow::Owned(hook.merge_request.into()),
            was_opened: hook.action == MergeRequestAction::Open,
            was_merged: hook.action == MergeRequestAction::Merge,
            is_open: hook.state == MergeRequestState::Open,
            date: hook.updated_at,
        }
    }
}

impl From<MergeRequestHook> for MergeRequestNoteInfo<'static> {
    fn from(hook: MergeRequestHook) -> Self {
        MergeRequestNoteInfo {
            note: hook.note.clone().unwrap().into(),
            merge_request: hook.into(),
        }
    }
}

/// A commit mentioned in an event payload.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Commit {
    /// The repository the commit belongs to.
    pub repo: Repo,
    /// The refname, if any, of the commit.
    pub refname: Option<String>,
    /// The ID of the commit.
    pub id: String,
    /// The ID of the last pipeline performed on the commit (if any).
    pub last_pipeline: Option<u64>,
}

impl From<Commit> for host::Commit {
    fn from(commit: Commit) -> Self {
        host::Commit {
            repo: commit.repo.into(),
            refname: commit.refname,
            id: CommitId::new(commit.id),
            last_pipeline: commit.last_pipeline,
        }
    }
}

/// Notification of a push into a repository.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct PushHook {
    /// The commit that was pushed.
    pub commit: Commit,
    /// The user that pushed the commit.
    pub author: User,
    /// When the push occurred..
    pub date: DateTime<Utc>,
}

impl From<PushHook> for PushInfo {
    fn from(hook: PushHook) -> Self {
        PushInfo {
            commit: hook.commit.into(),
            author: hook.author.into(),
            date: hook.date,
        }
    }
}

/// Notification of project creation.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct ProjectHook {
    /// The project that was created.
    pub project: Repo,
}

/// The action performed on the membership set.
#[derive(Deserialize, Serialize, Debug, Clone, Copy, PartialEq, Eq)]
pub enum MembershipAction {
    /// A user was added to a membership set.
    #[serde(rename = "added")]
    Added,
    /// A user was removed from a membership set.
    #[serde(rename = "removed")]
    Removed,
}

/// The access level granted to a user.
#[derive(Deserialize, Serialize, Debug, Clone, Copy, PartialEq, Eq)]
pub enum AccessLevel {
    /// The user is a contributor (no special permissions).
    #[serde(rename = "contributor")]
    Contributor,
    /// The user is a reporter (issue and triaging permissions).
    #[serde(rename = "reporter")]
    Reporter,
    /// The user is a developer (code-level permissions).
    #[serde(rename = "developer")]
    Developer,
    /// The user is a maintainer (target branch permissions)
    #[serde(rename = "maintainer")]
    Maintainer,
    /// The user is an owner (project administration permissions).
    #[serde(rename = "owner")]
    Owner,
}

impl From<AccessLevel> for ghostflow_ext::AccessLevel {
    fn from(access: AccessLevel) -> Self {
        match access {
            AccessLevel::Contributor => ghostflow_ext::AccessLevel::Contributor,
            AccessLevel::Reporter => ghostflow_ext::AccessLevel::Reporter,
            AccessLevel::Developer => ghostflow_ext::AccessLevel::Developer,
            AccessLevel::Maintainer => ghostflow_ext::AccessLevel::Maintainer,
            AccessLevel::Owner => ghostflow_ext::AccessLevel::Owner,
        }
    }
}

impl From<ghostflow_ext::AccessLevel> for AccessLevel {
    fn from(access: ghostflow_ext::AccessLevel) -> Self {
        match access {
            ghostflow_ext::AccessLevel::Contributor => AccessLevel::Contributor,
            ghostflow_ext::AccessLevel::Reporter => AccessLevel::Reporter,
            ghostflow_ext::AccessLevel::Developer => AccessLevel::Developer,
            ghostflow_ext::AccessLevel::Maintainer => AccessLevel::Maintainer,
            ghostflow_ext::AccessLevel::Owner => AccessLevel::Owner,
        }
    }
}

/// Notification about a membership change.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct MembershipHook {
    /// The action on the membership list.
    pub action: MembershipAction,
    /// The repository whose membership set changed.
    pub repo: Repo,
    /// The user whose membership changed.
    pub user: User,
    /// The access level granted (or revoked).
    pub access: AccessLevel,
}

impl From<MembershipHook> for MembershipAdditionInfo {
    fn from(hook: MembershipHook) -> Self {
        MembershipAdditionInfo {
            repo: hook.repo.into(),
            membership: ghostflow_ext::Membership {
                user: hook.user.into(),
                access_level: hook.access.into(),
                expiration: None,
            },
        }
    }
}

impl From<MembershipHook> for MembershipRemovalInfo {
    fn from(hook: MembershipHook) -> Self {
        MembershipRemovalInfo {
            repo: hook.repo.into(),
            user: hook.user.into(),
        }
    }
}
