// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::borrow::Cow;

use log::error;

use crate::actions::merge_requests::utils;
use crate::actions::merge_requests::{Action, ActionError, Data, Effect};

/// The unstage action
///
/// Removes a merge request topic from the main branch's stage.
#[derive(Debug, Default, Clone, Copy)]
pub struct Unstage;

impl Action for Unstage {
    fn help(&self, data: &Data) -> Option<Cow<'static, str>> {
        let _ = data.main_branch.stage()?;

        let msg = "Removes the merge request topic from the stage.";

        Some(msg.into())
    }

    fn perform(&self, arguments: &[&str], data: &Data) -> Vec<Effect> {
        let mut effects = Vec::new();

        let action = if let Some(action) = data.main_branch.stage() {
            action
        } else {
            effects.push(ActionError::not_supported().into());
            return effects;
        };
        if !data.action_data.cause.is_submitter && data.action_data.cause.how < action.access_level
        {
            effects.push(ActionError::insufficient_permissions(action.access_level).into());
            return effects;
        }

        let matches = utils::command_app("unstage").try_get_matches_from(arguments);
        let _ = match matches {
            Ok(matches) => matches,
            Err(err) => {
                effects.push(ActionError::unrecognized_arguments(err).into());
                return effects;
            },
        };

        let mr = &data.info.merge_request;

        let mut stage = action.stage();
        let res = stage.unstage_merge_request(mr);

        if let Err(err) = res {
            error!(
                target: "ghostflow-director/handler",
                "failed during the unstage action on {}: {:?}",
                mr.url,
                err,
            );

            let msg = format!(
                "Error occurred during unstage action (@{}): {}",
                data.action_data.project.maintainers().join(" @"),
                err,
            );
            effects.push(Effect::error(msg));
        }

        effects
    }
}

#[cfg(test)]
mod tests {
    use serde_json::json;

    use crate::handlers::test::*;

    const MERGE_REQUEST_TOPIC: &str = "6f781d4d4d85dfd5a609532a26bcc6fd63fcef51";

    fn stage_config() -> serde_json::Value {
        json!({
            "required_access_level": "developer",
        })
    }

    #[test]
    fn test_command_unstage_refuse_not_configured() {
        let mut service = TestService::new(
            "test_command_unstage_refuse_not_configured",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: unstage"),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": {},
                },
            },
        });
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `unstage` command: the command is not supported"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_unstage_disallowed() {
        let mut service = TestService::new(
            "test_command_unstage_disallowed",
            [
                Action::create_user("fork"),
                Action::create_user("unrelated"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("unrelated", "Do: unstage"),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "stage": stage_config(),
                    },
                },
            },
        });
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `unstage` command: permission denied: insufficient access \
                   level (Developer)"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_unstage_bad_arguments() {
        let mut service = TestService::new(
            "test_command_unstage_bad_arguments",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: unstage --unrecognized"),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "stage": stage_config(),
                    },
                },
            },
        });
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `unstage` command: unrecognized arguments: error: \
                   unexpected argument '--unrecognized' found"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_unstage_submitter_allowed() {
        let mut service = TestService::new(
            "test_command_unstage_submitter_allowed",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false), // Dummy MR to avoid an id of 0
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: stage"),
                Action::mr_comment("fork", "Do: unstage"),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "stage": stage_config(),
                    },
                },
            },
        });
        let end = EndSignal::MergeRequestComment {
            content: "This merge request has been unstaged upon request.".into(),
        };

        service.launch(config, end).unwrap();
    }
}
