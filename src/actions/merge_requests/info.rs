// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Common data structures required for merge request actions.
//!
//! This module contains structures necessary to carry out the actual actions needed for merge
//! requests.

use std::borrow::Cow;
use std::collections::hash_map::HashMap;

use chrono::{DateTime, Utc};
use ghostflow::host::{CheckStatus, HostingService, HostingServiceError, MergeRequest};
use ghostflow::utils::TrailerRef;
use git_workarea::{CommitId, GitContext, GitError};
use serde_json::{json, Value};
use thiserror::Error;

/// Errors which may occur when handling `Backport` trailers.
#[derive(Debug, Error)]
pub enum BackportError {
    /// Failure to parse a revision in a specification.
    #[error("failed to parse commit {}: {}", rev, output)]
    RevParse {
        /// The commit that was parsed.
        rev: CommitId,
        /// The stderr output of `git rev-parse`.
        output: String,
    },
    /// Failure to run a `git` command.
    #[error("git error: {}", source)]
    GitError {
        /// The error from `git`.
        #[from]
        source: GitError,
    },
}

impl BackportError {
    fn rev_parse(rev: CommitId, output: &[u8]) -> Self {
        Self::RevParse {
            rev,
            output: String::from_utf8_lossy(output).into(),
        }
    }
}

type BackportResult<T> = Result<T, BackportError>;

/// How a merge request should be merged into a target branch.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum MergeStrategy {
    /// Use a merge commit.
    Merge,
    /// Fast-forward to the given commit.
    FastForward,
}

/// Information about backporting a merge request into multiple branches.
#[derive(Debug, Clone)]
pub struct Backport {
    /// The branch to backport into.
    branch: String,
    /// The commit to backport.
    commit: Option<CommitId>,
    /// The strategy to use when merging this branch.
    strategy: MergeStrategy,
}

impl Backport {
    /// Parse backport information from a string description.
    ///
    /// The format is `branch[:commit]`. Without the `commit` part, the last commit of the merge
    /// request's source topic is implied.
    // Until the `!` type is stable, an ergonomic and obviously-safe way to implement this
    // with the `FromStr` trait is not available.
    #[allow(clippy::should_implement_trait)]
    pub fn from_str<S>(value: S) -> Self
    where
        S: AsRef<str>,
    {
        Self::from_str_impl(value.as_ref())
    }

    /// Backport parsing implementation.
    fn from_str_impl(value: &str) -> Self {
        let raw_value = if value.starts_with('`') && value.ends_with('`') {
            &value[1..value.len() - 1]
        } else {
            value
        };

        let (branch, commit) = if let Some(loc) = raw_value.bytes().position(|c| c == b':') {
            let (branch, commit_part) = raw_value.split_at(loc);
            let (_, commit) = commit_part.split_at(1);
            (branch, Some(commit))
        } else {
            (raw_value, None)
        };

        Self {
            branch: branch.into(),
            commit: commit.map(CommitId::new),
            strategy: MergeStrategy::Merge,
        }
    }

    /// The name of the targeted backport branch.
    pub fn branch(&self) -> &str {
        &self.branch
    }

    /// The commit to backport.
    pub fn commit(&self, mr: &MergeRequest, ctx: &GitContext) -> BackportResult<CommitId> {
        self.commit
            .as_ref()
            .map(|commit_id| {
                let rev = commit_id.as_str().replace("HEAD", mr.commit.id.as_str());
                let rev_parse = ctx
                    .git()
                    .arg("rev-parse")
                    .arg("--verify")
                    .arg(format!("{}^{{commit}}", rev))
                    .output()
                    .map_err(|err| GitError::subcommand("rev-parse", err))?;
                if !rev_parse.status.success() {
                    return Err(BackportError::rev_parse(
                        CommitId::new(rev),
                        &rev_parse.stderr,
                    ));
                }
                let commit_id = String::from_utf8_lossy(&rev_parse.stdout);
                Ok(CommitId::new(commit_id.trim()))
            })
            .unwrap_or_else(|| Ok(mr.commit.id.clone()))
    }

    /// Whether the backport should use the fast-forward topology or not.
    pub fn fast_forward(&self) -> bool {
        matches!(self.strategy, MergeStrategy::FastForward)
    }
}

/// Errors which may occur when extracting information from a merge request.
#[derive(Debug, Error)]
pub enum MergeRequestInfoError {
    /// Failure to parse backport information.
    #[error("backport error: {}", source)]
    Backport {
        /// The source of the error.
        #[from]
        source: BackportError,
    },
    /// The hosting service returned an error.
    #[error("hosting service error: {}", source)]
    HostingService {
        /// The source of the error.
        #[from]
        source: HostingServiceError,
    },
}

type MergeRequestInfoResult<T> = Result<T, MergeRequestInfoError>;

/// Information required to handle a merge request update.
#[derive(Debug, Clone)]
pub struct Info<'a> {
    /// The merge request information.
    pub merge_request: Cow<'a, MergeRequest>,
    /// Indicates whether the MR was just opened or not.
    pub was_opened: bool,
    /// Indicates whether the MR was just merged or not.
    pub was_merged: bool,
    /// Whether the merge request is open or not.
    pub is_open: bool,
    /// When the merge request was last updated.
    pub date: DateTime<Utc>,
}

impl Info<'_> {
    fn trailers(&self) -> Vec<TrailerRef> {
        Self::trailers_for(&self.merge_request)
    }

    fn trailers_for(merge_request: &MergeRequest) -> Vec<TrailerRef> {
        TrailerRef::extract(&merge_request.description)
    }

    /// Returns `true` if it appears that the source branch has been deleted.
    pub fn is_source_branch_deleted(&self) -> bool {
        let id = self.merge_request.commit.id.as_str();
        id.is_empty() || id == "0000000000000000000000000000000000000000"
    }

    /// Whether to use the fast-forward merge topology for the main branch target.
    pub fn fast_forward(&self) -> bool {
        self.trailers()
            .into_iter()
            .filter_map(|trailer| {
                if trailer.token == "Fast-forward" || trailer.token == "fast-forward" {
                    Some(trailer.value == "true")
                } else {
                    None
                }
            })
            .next()
            .unwrap_or(false)
    }

    /// The name of the topic for the merge request according to its description.
    pub fn topic_rename(&self) -> Option<&str> {
        self.trailers()
            .into_iter()
            .filter_map(|trailer| {
                if trailer.token == "Topic-rename" || trailer.token == "topic-rename" {
                    Some(trailer.value)
                } else {
                    None
                }
            })
            .next()
    }

    /// The name of the topic for the merge request for use in actions.
    pub fn topic_name(&self) -> &str {
        self.topic_rename()
            .unwrap_or(&self.merge_request.source_branch)
    }

    /// The raw backport information for a merge request.
    pub fn backports_for(merge_request: &MergeRequest) -> Vec<Backport> {
        let mut strategy = MergeStrategy::Merge;

        let mut backports = Self::trailers_for(merge_request)
            .into_iter()
            .filter_map(|trailer| {
                if trailer.token == "Backport" || trailer.token == "backport" {
                    Some(Backport::from_str(trailer.value))
                } else if trailer.token == "Backport-ff" || trailer.token == "backport-ff" {
                    let mut backport = Backport::from_str(trailer.value);
                    backport.strategy = MergeStrategy::FastForward;
                    Some(backport)
                } else if trailer.token == "Fast-forward" || trailer.token == "fast-forward" {
                    if trailer.value == "true" {
                        strategy = MergeStrategy::FastForward;
                    }
                    None
                } else {
                    None
                }
            })
            .collect::<Vec<_>>();

        backports.push(Backport {
            branch: merge_request.target_branch.clone(),
            commit: None,
            strategy,
        });

        backports
    }

    /// Backport information for the merge request.
    ///
    /// Backport information is stored in the description of the merge request using `Backport`
    /// trailers.
    pub fn backports(&self) -> Vec<Backport> {
        Self::backports_for(&self.merge_request)
    }

    /// Backport map
    pub fn backports_map(
        merge_request: &MergeRequest,
        ctx: &GitContext,
    ) -> HashMap<String, (BackportResult<CommitId>, MergeStrategy)> {
        Self::backports_for(merge_request)
            .into_iter()
            .map(|backport| {
                let commit = backport.commit(merge_request, ctx);
                (backport.branch, (commit, backport.strategy))
            })
            .collect()
    }

    /// Backport information in a JSON format.
    ///
    /// Backports which are invalid are given as `null`.
    pub fn backports_json(&self, ctx: &GitContext) -> Value {
        json!(self
            .backports()
            .into_iter()
            .map(|backport| {
                let commit = backport
                    .commit(&self.merge_request, ctx)
                    .ok()
                    .map(|commit| commit.as_str().to_string());
                (backport.branch, commit)
            })
            .collect::<HashMap<_, _>>())
    }

    /// Determine the check status of a merge request.
    ///
    /// This will check the status messages for each target branch according to the backport
    /// information to ensure that it has been checked according to the backport information
    /// available for the merge request.
    pub fn check_status<F>(
        &self,
        service: &dyn HostingService,
        ctx: &GitContext,
        status_name_fn: F,
    ) -> MergeRequestInfoResult<CheckStatus>
    where
        F: Fn(&str) -> String,
    {
        // If the branch has been updated for any reason, rerun the checks.
        if self.was_opened || self.merge_request.old_commit.is_some() {
            return Ok(CheckStatus::Unchecked);
        }

        let service_user_handle = &service.service_user().handle;
        let status_map = service
            .get_commit_statuses(&self.merge_request.commit)?
            .into_iter()
            // Only look at statuses posted by the current user.
            .filter(|status| &status.author.handle == service_user_handle)
            // Make a mapping from status name to status.
            .map(|status| (status.name.clone(), status))
            .collect::<HashMap<_, _>>();
        let check_statuses = self
            .backports()
            .into_iter()
            .map(|backport| {
                // The name of the status we should expect.
                let status_name = status_name_fn(&backport.branch);
                // The commit that we expect to have been checked.
                let expected_commit = backport.commit(&self.merge_request, ctx)?;
                Ok(status_map
                    .get(&status_name)
                    .and_then(|status| {
                        // Extract the trailers from the status description.
                        TrailerRef::extract(&status.description)
                            .into_iter()
                            .filter_map(|trailer| {
                                if trailer.token == "Branch-at" {
                                    // We have the commit that we checked.
                                    Some((status, trailer.value))
                                } else {
                                    None
                                }
                            })
                            .next()
                    })
                    .and_then(|(status, branch_at)| {
                        if expected_commit.as_str() == branch_at {
                            // We checked the same commit we have.
                            Some(status.state.into())
                        } else {
                            None
                        }
                    })
                    // If we don't have a state, we haven't performed the check as intended.
                    .unwrap_or(CheckStatus::Unchecked))
            })
            .collect::<MergeRequestInfoResult<Vec<_>>>()?;

        Ok(if check_statuses.iter().all(|cs| cs.is_ok()) {
            CheckStatus::Pass
        } else if check_statuses.iter().all(|cs| cs.is_checked()) {
            CheckStatus::Fail
        } else {
            CheckStatus::Unchecked
        })
    }
}

#[cfg(test)]
mod test {
    use std::borrow::Cow;

    use chrono::Utc;
    use ghostflow::host::{Commit, MergeRequest, Repo, User};
    use git_workarea::{CommitId, GitContext};

    use crate::actions::merge_requests::{Backport, BackportError, Info, MergeStrategy};

    const COMMIT: &str = "4ab2a22a300d1425c5033a6429a44d17bb639a11";

    fn commit_context_impl(desc: &str, commit: &str) -> (MergeRequest, GitContext) {
        let git_path = concat!(env!("CARGO_MANIFEST_DIR"), "/.git");
        let repo = Repo {
            name: "self".into(),
            url: git_path.into(),
            http_url: git_path.into(),
            forked_from: None,
        };
        let user = User {
            handle: "user".into(),
            name: "user".into(),
            email: "user@example.com".into(),
        };
        let mr = MergeRequest {
            source_repo: Some(repo.clone()),
            source_branch: "topic".into(),
            target_repo: repo.clone(),
            target_branch: "master".into(),
            id: 1,
            url: git_path.into(),
            work_in_progress: false,
            description: desc.into(),
            old_commit: None,
            commit: Commit {
                repo,
                id: CommitId::new(commit),
                refname: None,
                last_pipeline: None,
            },
            author: user,

            reference: "!1".into(),
            remove_source_branch: false,
        };
        let ctx = GitContext::new(git_path);

        (mr, ctx)
    }

    fn commit_context() -> (MergeRequest, GitContext) {
        commit_context_impl("MR description", COMMIT)
    }

    fn parse_backport(value: &str) -> Backport {
        Backport::from_str(value)
    }

    #[test]
    fn test_backport_branch() {
        let backport = parse_backport("target_branch");
        assert_eq!(backport.branch, "target_branch");
        assert_eq!(backport.commit, None);
        assert_eq!(backport.strategy, MergeStrategy::Merge);
    }

    #[test]
    fn test_backport_branch_markup() {
        let backport = parse_backport("`target_branch`");
        assert_eq!(backport.branch, "target_branch");
        assert_eq!(backport.commit, None);
        assert_eq!(backport.strategy, MergeStrategy::Merge);
    }

    #[test]
    fn test_backport_branch_bad_markup() {
        let backport = parse_backport("target_branch`");
        assert_eq!(backport.branch, "target_branch`");
        assert_eq!(backport.commit, None);
        assert_eq!(backport.strategy, MergeStrategy::Merge);

        let backport = parse_backport("`target_branch");
        assert_eq!(backport.branch, "`target_branch");
        assert_eq!(backport.commit, None);
        assert_eq!(backport.strategy, MergeStrategy::Merge);

        let backport = parse_backport("target`_branch");
        assert_eq!(backport.branch, "target`_branch");
        assert_eq!(backport.commit, None);
        assert_eq!(backport.strategy, MergeStrategy::Merge);

        let backport = parse_backport("`target`_branch");
        assert_eq!(backport.branch, "`target`_branch");
        assert_eq!(backport.commit, None);
        assert_eq!(backport.strategy, MergeStrategy::Merge);
    }

    #[test]
    fn test_backport_commit_spec() {
        let backport = parse_backport("target_branch:HEAD");
        assert_eq!(backport.branch, "target_branch");
        assert_eq!(backport.commit, Some(CommitId::new("HEAD")));
        assert_eq!(backport.strategy, MergeStrategy::Merge);
    }

    #[test]
    fn test_backport_commit_spec_colon() {
        let backport = parse_backport("target_branch:HEAD:with:colons");
        assert_eq!(backport.branch, "target_branch");
        assert_eq!(backport.commit, Some(CommitId::new("HEAD:with:colons")));
        assert_eq!(backport.strategy, MergeStrategy::Merge);
    }

    #[test]
    fn test_backport_commit_relative() {
        let backport = parse_backport("target_branch:HEAD^2");
        let (mr, ctx) = commit_context();

        let commit = backport.commit(&mr, &ctx).unwrap();
        assert_eq!(
            commit,
            CommitId::new("6781ccf2e07a1c15f8abd922f4d8ae765b10b1c2"),
        );
    }

    #[test]
    fn test_backport_commit_no_exist() {
        let backport = parse_backport("target_branch:deadbeefdeadbeefdeadbeefdeadbeefdeadbeef");
        let (mr, ctx) = commit_context();

        let err = backport.commit(&mr, &ctx).unwrap_err();

        if let BackportError::RevParse {
            rev,
            output,
        } = err
        {
            assert_eq!(rev.as_str(), "deadbeefdeadbeefdeadbeefdeadbeefdeadbeef");
            assert_eq!(output, "fatal: Needed a single revision\n");
        } else {
            panic!("unexpected error: {:?}", err);
        }
    }

    #[test]
    fn test_backport_commit_no_exist_relative() {
        let backport = parse_backport("target_branch:HEAD^3");
        let (mr, ctx) = commit_context();

        let err = backport.commit(&mr, &ctx).unwrap_err();

        if let BackportError::RevParse {
            rev,
            output,
        } = err
        {
            assert_eq!(rev.as_str(), "4ab2a22a300d1425c5033a6429a44d17bb639a11^3");
            assert_eq!(output, "fatal: Needed a single revision\n");
        } else {
            panic!("unexpected error: {:?}", err);
        }
    }

    fn merge_request_info(mr: &MergeRequest) -> Info {
        Info {
            merge_request: Cow::Borrowed(mr),
            was_opened: false,
            was_merged: false,
            is_open: true,
            date: Utc::now(),
        }
    }

    #[test]
    fn test_mr_info_deleted() {
        let mr = commit_context_impl("", "").0;
        let mr_info = merge_request_info(&mr);
        assert!(mr_info.is_source_branch_deleted());

        let mr = commit_context_impl("", "0000000000000000000000000000000000000000").0;
        let mr_info = merge_request_info(&mr);
        assert!(mr_info.is_source_branch_deleted());

        let mr = commit_context_impl("", COMMIT).0;
        let mr_info = merge_request_info(&mr);
        assert!(!mr_info.is_source_branch_deleted());
    }

    #[test]
    fn test_mr_info_fast_forward() {
        let mr = commit_context_impl("", COMMIT).0;
        let mr_info = merge_request_info(&mr);
        assert!(!mr_info.fast_forward());

        let mr = commit_context_impl("Fast-forward: true", COMMIT).0;
        let mr_info = merge_request_info(&mr);
        assert!(mr_info.fast_forward());

        let mr = commit_context_impl("fast-forward: true", COMMIT).0;
        let mr_info = merge_request_info(&mr);
        assert!(mr_info.fast_forward());

        let mr = commit_context_impl("fast-forward: blah", COMMIT).0;
        let mr_info = merge_request_info(&mr);
        assert!(!mr_info.fast_forward());
    }

    #[test]
    fn test_mr_info_topic_rename() {
        let mr = commit_context_impl("", COMMIT).0;
        let mr_info = merge_request_info(&mr);
        assert_eq!(mr_info.topic_rename(), None);
        assert_eq!(mr_info.topic_name(), "topic");

        let mr = commit_context_impl("Topic-rename: new-name", COMMIT).0;
        let mr_info = merge_request_info(&mr);
        assert_eq!(mr_info.topic_rename(), Some("new-name"));
        assert_eq!(mr_info.topic_name(), "new-name");

        let mr = commit_context_impl("topic-rename: new-name", COMMIT).0;
        let mr_info = merge_request_info(&mr);
        assert_eq!(mr_info.topic_rename(), Some("new-name"));
        assert_eq!(mr_info.topic_name(), "new-name");

        let mr = commit_context_impl("Topic-rename: new-name\nTopic-rename: other-name", COMMIT).0;
        let mr_info = merge_request_info(&mr);
        assert_eq!(mr_info.topic_rename(), Some("new-name"));
        assert_eq!(mr_info.topic_name(), "new-name");
    }

    #[test]
    fn test_mr_info_backport() {
        let mr = commit_context_impl("", COMMIT).0;
        let mr_info = merge_request_info(&mr);
        let backports = mr_info.backports();
        assert_eq!(backports.len(), 1);
        assert_eq!(backports[0].branch, "master");
        assert_eq!(backports[0].commit, None);
        assert_eq!(backports[0].strategy, MergeStrategy::Merge);

        let mr = commit_context_impl("Backport: release", COMMIT).0;
        let mr_info = merge_request_info(&mr);
        let backports = mr_info.backports();
        assert_eq!(backports.len(), 2);
        assert_eq!(backports[0].branch, "release");
        assert_eq!(backports[0].commit, None);
        assert_eq!(backports[0].strategy, MergeStrategy::Merge);
        assert_eq!(backports[1].branch, "master");
        assert_eq!(backports[1].commit, None);
        assert_eq!(backports[1].strategy, MergeStrategy::Merge);

        let mr = commit_context_impl("backport: release", COMMIT).0;
        let mr_info = merge_request_info(&mr);
        let backports = mr_info.backports();
        assert_eq!(backports.len(), 2);
        assert_eq!(backports[0].branch, "release");
        assert_eq!(backports[0].commit, None);
        assert_eq!(backports[0].strategy, MergeStrategy::Merge);
        assert_eq!(backports[1].branch, "master");
        assert_eq!(backports[1].commit, None);
        assert_eq!(backports[1].strategy, MergeStrategy::Merge);

        let mr = commit_context_impl("Backport: release:HEAD", COMMIT).0;
        let mr_info = merge_request_info(&mr);
        let backports = mr_info.backports();
        assert_eq!(backports.len(), 2);
        assert_eq!(backports[0].branch, "release");
        assert_eq!(backports[0].commit, Some(CommitId::new("HEAD")));
        assert_eq!(backports[0].strategy, MergeStrategy::Merge);
        assert_eq!(backports[1].branch, "master");
        assert_eq!(backports[1].commit, None);
        assert_eq!(backports[1].strategy, MergeStrategy::Merge);

        let mr = commit_context_impl("Backport: release:HEAD\nBackport: old-release", COMMIT).0;
        let mr_info = merge_request_info(&mr);
        let backports = mr_info.backports();
        assert_eq!(backports.len(), 3);
        assert_eq!(backports[0].branch, "release");
        assert_eq!(backports[0].commit, Some(CommitId::new("HEAD")));
        assert_eq!(backports[0].strategy, MergeStrategy::Merge);
        assert_eq!(backports[1].branch, "old-release");
        assert_eq!(backports[1].commit, None);
        assert_eq!(backports[1].strategy, MergeStrategy::Merge);
        assert_eq!(backports[2].branch, "master");
        assert_eq!(backports[2].commit, None);
        assert_eq!(backports[2].strategy, MergeStrategy::Merge);
    }

    #[test]
    fn test_mr_info_backport_ff() {
        let mr = commit_context_impl("", COMMIT).0;
        let mr_info = merge_request_info(&mr);
        let backports = mr_info.backports();
        assert_eq!(backports.len(), 1);
        assert_eq!(backports[0].branch, "master");
        assert_eq!(backports[0].commit, None);
        assert_eq!(backports[0].strategy, MergeStrategy::Merge);

        let mr = commit_context_impl("Fast-forward: true", COMMIT).0;
        let mr_info = merge_request_info(&mr);
        let backports = mr_info.backports();
        assert_eq!(backports.len(), 1);
        assert_eq!(backports[0].branch, "master");
        assert_eq!(backports[0].commit, None);
        assert_eq!(backports[0].strategy, MergeStrategy::FastForward);

        let mr = commit_context_impl("fast-forward: true", COMMIT).0;
        let mr_info = merge_request_info(&mr);
        let backports = mr_info.backports();
        assert_eq!(backports.len(), 1);
        assert_eq!(backports[0].branch, "master");
        assert_eq!(backports[0].commit, None);
        assert_eq!(backports[0].strategy, MergeStrategy::FastForward);

        let mr = commit_context_impl("Backport-ff: release", COMMIT).0;
        let mr_info = merge_request_info(&mr);
        let backports = mr_info.backports();
        assert_eq!(backports.len(), 2);
        assert_eq!(backports[0].branch, "release");
        assert_eq!(backports[0].commit, None);
        assert_eq!(backports[0].strategy, MergeStrategy::FastForward);
        assert_eq!(backports[1].branch, "master");
        assert_eq!(backports[1].commit, None);
        assert_eq!(backports[1].strategy, MergeStrategy::Merge);

        let mr = commit_context_impl("backport-ff: release", COMMIT).0;
        let mr_info = merge_request_info(&mr);
        let backports = mr_info.backports();
        assert_eq!(backports.len(), 2);
        assert_eq!(backports[0].branch, "release");
        assert_eq!(backports[0].commit, None);
        assert_eq!(backports[0].strategy, MergeStrategy::FastForward);
        assert_eq!(backports[1].branch, "master");
        assert_eq!(backports[1].commit, None);
        assert_eq!(backports[1].strategy, MergeStrategy::Merge);

        let mr = commit_context_impl("Backport-ff: release:HEAD", COMMIT).0;
        let mr_info = merge_request_info(&mr);
        let backports = mr_info.backports();
        assert_eq!(backports.len(), 2);
        assert_eq!(backports[0].branch, "release");
        assert_eq!(backports[0].commit, Some(CommitId::new("HEAD")));
        assert_eq!(backports[0].strategy, MergeStrategy::FastForward);
        assert_eq!(backports[1].branch, "master");
        assert_eq!(backports[1].commit, None);
        assert_eq!(backports[1].strategy, MergeStrategy::Merge);

        let mr = commit_context_impl("Backport-ff: release:HEAD\nBackport: old-release", COMMIT).0;
        let mr_info = merge_request_info(&mr);
        let backports = mr_info.backports();
        assert_eq!(backports.len(), 3);
        assert_eq!(backports[0].branch, "release");
        assert_eq!(backports[0].commit, Some(CommitId::new("HEAD")));
        assert_eq!(backports[0].strategy, MergeStrategy::FastForward);
        assert_eq!(backports[1].branch, "old-release");
        assert_eq!(backports[1].commit, None);
        assert_eq!(backports[1].strategy, MergeStrategy::Merge);
        assert_eq!(backports[2].branch, "master");
        assert_eq!(backports[2].commit, None);
        assert_eq!(backports[2].strategy, MergeStrategy::Merge);
    }
}
