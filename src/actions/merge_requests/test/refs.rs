// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::borrow::Cow;

use clap::{Arg, ArgAction};
use ghostflow::actions::test::refs::{TestRefs, TestRefsError};

use crate::actions::merge_requests::utils;
use crate::actions::merge_requests::{Action, ActionError, Data, Effect, InnerActionError};

impl InnerActionError for TestRefsError {}

#[derive(Debug, Clone, Copy)]
pub struct TestRefsAction<'a> {
    test_refs: &'a TestRefs,
}

impl<'a> TestRefsAction<'a> {
    pub fn new(test_refs: &'a TestRefs) -> Self {
        Self {
            test_refs,
        }
    }
}

impl Action for TestRefsAction<'_> {
    fn help(&self, _: &Data) -> Option<Cow<'static, str>> {
        let msg =
            "Creates a special reference on the repository which indicates that the merge request \
             should be tested. Test machines look at these special references and run the tests. \
             The `--stop` argument may be given to remove the special reference. The reference is \
             also removed when the merge request is closed (for any reason). Updates to the merge \
             request leave any existing test reference in place and require either a new `Do: \
             test` to update it to the new code or a `Do: test --stop` to remove it.";

        Some(msg.into())
    }

    fn perform(&self, arguments: &[&str], data: &Data) -> Vec<Effect> {
        let mut effects = Vec::new();

        let matches = utils::command_app("test")
            .arg(Arg::new("STOP").long("stop").action(ArgAction::SetTrue))
            .try_get_matches_from(arguments);
        let matches = match matches {
            Ok(matches) => matches,
            Err(err) => {
                effects.push(ActionError::unrecognized_arguments(err).into());
                return effects;
            },
        };

        // TODO: Support testing backport branches. There are issues here where a backport
        // configuration is deleted. How do we know to untest it for the now-deleted branch?
        // Merging into the target branches should also be supported.

        let res = if matches.get_flag("STOP") {
            self.test_refs.untest_mr(&data.info.merge_request)
        } else {
            self.test_refs.test_mr(&data.info.merge_request)
        };

        if let Err(err) = res {
            effects.push(ActionError::from(err).into());
        }

        effects
    }
}

#[cfg(test)]
mod tests {
    use git_workarea::CommitId;
    use serde_json::{json, Value};
    use serial_test::serial;

    use crate::handlers::test::*;

    const MERGE_REQUEST_TOPIC: &str = "6f781d4d4d85dfd5a609532a26bcc6fd63fcef51";

    #[test]
    fn test_command_test_refuse_not_configured() {
        let mut service = TestService::new(
            "test_command_test_refuse_not_configured",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test"),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": {},
                },
            },
        });
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `test` command: the command is not supported"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    fn test_refs_config() -> Value {
        json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "test": {
                            "required_access_level": "maintainer",
                            "backend": "refs",
                        },
                    },
                },
            },
        })
    }

    #[test]
    fn test_command_test_refs_bad_arguments() {
        let mut service = TestService::new(
            "test_command_test_refs_bad_arguments",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test --unrecognized"),
            ],
        )
        .unwrap();
        let config = test_refs_config();
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `test` command: unrecognized arguments: error: \
                   unexpected argument '--unrecognized' found"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_test_refs() {
        let mut service = TestService::new(
            "test_command_test_refs",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test"),
            ],
        )
        .unwrap();
        let config = test_refs_config();
        let end = EndSignal::Push {
            project: "ghostflow/example".into(),
            ref_: "refs/test-topics/0".into(),
            sha1: CommitId::new(MERGE_REQUEST_TOPIC),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    #[serial]
    fn test_command_test_refs_stop() {
        let mut service = TestService::new(
            "test_command_test_refs_stop",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test"),
                Action::mr_comment("ghostflow", "Do: test --stop"),
            ],
        )
        .unwrap();
        let config = test_refs_config();
        let end = EndSignal::Push {
            project: "ghostflow/example".into(),
            ref_: "refs/test-topics/0".into(),
            sha1: CommitId::new("0000000000000000000000000000000000000000"),
        };

        service.launch(config, end).unwrap();
    }
}
