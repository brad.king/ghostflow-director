// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::borrow::Cow;

use clap::{Arg, ArgAction};
use ghostflow::actions::merge::{self, MergeActionResult, MergeBackport};
use log::{error, info};
use thiserror::Error;

use crate::actions::merge_requests::info::BackportError;
use crate::actions::merge_requests::utils;
use crate::actions::merge_requests::{Action, ActionError, Data, Effect, InnerActionError};
use crate::ghostflow_ext::AccessLevel;

#[derive(Debug, Error)]
enum MergeCommandError {
    #[error("failed to find the commit to backport into {}: {}", branch, source)]
    NoCommit {
        branch: String,
        #[source]
        source: BackportError,
    },
    #[error(
        "the `{}` branch does not support merging (with the specified merge topology)",
        branch
    )]
    NoMerge { branch: String },
    #[error("the `{}` branch does not support backports", branch)]
    NoBackports { branch: String },
    #[error("failed to merge: {}", source)]
    Merge {
        #[from]
        source: merge::MergeError,
    },
}

impl MergeCommandError {
    fn no_commit(branch: String, source: BackportError) -> Self {
        MergeCommandError::NoCommit {
            branch,
            source,
        }
    }

    fn no_merge(branch: String) -> Self {
        MergeCommandError::NoMerge {
            branch,
        }
    }

    fn no_backports(branch: String) -> Self {
        MergeCommandError::NoBackports {
            branch,
        }
    }
}

#[derive(Debug, Error)]
enum MergeCommandServiceError {
    #[error("failed to add service user to project: {}", source)]
    AddServiceUser {
        #[source]
        /// The source of the error.
        source: ghostflow::host::HostingServiceError,
    },
}

impl MergeCommandServiceError {
    fn add_service_user(source: ghostflow::host::HostingServiceError) -> Self {
        Self::AddServiceUser {
            source,
        }
    }
}

impl InnerActionError for MergeCommandServiceError {}

/// The merge action
///
/// Merges a merge request's topic into the set of target branches while collecting trailers for
/// the merge commit message from the merge request comment stream and state.
#[derive(Debug, Default, Clone, Copy)]
pub struct Merge;

impl Action for Merge {
    fn help(&self, data: &Data) -> Option<Cow<'static, str>> {
        let _ = data.main_branch.merge()?;

        let msg =
            "Merges the merge request topic into the target branch (or branches in the case of \
             backports). It fails if the merge request has not passed the checks or it is \
             considered a work-in-progress. The name of the topic merged into the target branch \
             may be changed with either the `--topic` argument or using a `Topic-rename` trailer \
             in the description.";

        Some(msg.into())
    }

    fn perform(&self, arguments: &[&str], data: &Data) -> Vec<Effect> {
        let mut effects = Vec::new();

        let mr_info = &data.info;
        let potential_action = if mr_info.fast_forward() {
            data.main_branch.merge_ff()
        } else {
            data.main_branch.merge()
        };

        let action = if let Some(action) = potential_action {
            action
        } else {
            effects.push(ActionError::not_supported().into());
            return effects;
        };
        if data.action_data.cause.how < action.access_level {
            effects.push(ActionError::insufficient_permissions(action.access_level).into());
            return effects;
        }

        if let Some(access) = action.service_access_level {
            let project = &data.action_data.project;
            let service_user = project.service.service_user();
            let current_access_level = project.access_level(service_user).unwrap_or_else(|err| {
                error!(
                    target: "ghostflow-director/handler",
                    "failed to get membership level of service user to {}: {:?}",
                    project.name(), err,
                );

                // Act as if we have no real power here.
                AccessLevel::Contributor
            });
            if current_access_level < access {
                info!(
                    target: "ghostflow-director/handler",
                    "adding service user to {} with access level {:?}",
                    project.name(), access,
                );
                if let Err(err) = project
                    .service
                    .add_member(project.name(), service_user, access)
                {
                    effects.push(
                        ActionError::from(MergeCommandServiceError::add_service_user(err)).into(),
                    );
                    return effects;
                }
            }
        }

        let matches = utils::command_app("merge")
            .arg(
                Arg::new("TOPIC")
                    .short('t')
                    .long("topic")
                    .action(ArgAction::Set),
            )
            .try_get_matches_from(arguments);
        let matches = match matches {
            Ok(matches) => matches,
            Err(err) => {
                effects.push(ActionError::unrecognized_arguments(err).into());
                return effects;
            },
        };

        let (check_effects, check_status) = data.check_status();
        effects.extend(check_effects);
        if let Some(check_status) = check_status {
            if check_status.is_ok() {
                // Do nothing.
            } else if check_status.is_checked() {
                let msg = "refusing to merge; topic is failing the checks";
                effects.push(Effect::error(msg));
                return effects;
            } else {
                let msg = "refusing to merge; topic is missing the checks";
                effects.push(Effect::error(msg));
                return effects;
            }
        } else {
            // Effects are part of `check_effects` already.
            return effects;
        }

        let topic_name = matches
            .get_one::<String>("TOPIC")
            .cloned()
            .unwrap_or_else(|| mr_info.topic_name().into());

        if data.action_data.project.branch_names.contains(&topic_name) {
            let msg = format!(
                "the name of the topic (`{}`) is the same as a branch in the project; the \
                 `--topic` option may be used to merge with a different name",
                topic_name,
            );
            effects.push(Effect::error(msg));
            return effects;
        }

        let backports = mr_info.backports();

        let merge = action.merge();
        let res = if backports.is_empty() {
            merge
                .merge_mr_named(
                    &mr_info.merge_request,
                    topic_name,
                    &data.action_data.cause.who.identity(),
                    data.action_data.cause.when,
                )
                .map_err(Into::into)
        } else {
            let merge_many = data.action_data.project.merge_many();
            backports
                .into_iter()
                .map(|backport| {
                    if let Some(branch) = data.action_data.project.branches.get(backport.branch()) {
                        let backport_action = if backport.fast_forward() {
                            branch.merge_ff()
                        } else {
                            branch.merge()
                        };
                        if let Some(backport_action) = backport_action {
                            let settings = backport_action.merge().settings();
                            let commit = backport
                                .commit(&mr_info.merge_request, &data.action_data.project.context)
                                .map_err(|err| {
                                    MergeCommandError::no_commit(backport.branch().into(), err)
                                })?;
                            Ok(MergeBackport::new(settings, Some(commit)))
                        } else {
                            Err(MergeCommandError::no_merge(backport.branch().into()))
                        }
                    } else {
                        Err(MergeCommandError::no_backports(backport.branch().into()))
                    }
                })
                .collect::<Result<Vec<_>, MergeCommandError>>()
                .and_then(|merge_backports| {
                    merge_many
                        .merge_mr_named(
                            &mr_info.merge_request,
                            topic_name,
                            &data.action_data.cause.who.identity(),
                            data.action_data.cause.when,
                            merge_backports,
                        )
                        .map_err(Into::into)
                })
        };

        match res {
            Ok(MergeActionResult::Success) | Ok(MergeActionResult::Failed) => (),
            // Defer the action if merging failed.
            Ok(MergeActionResult::PushFailed) => {
                effects.push(Effect::error("failed to push the resulting merge"));
                effects.push(Effect::defer_handling());
                return effects;
            },
            Err(err) => {
                error!(
                    target: "ghostflow-director/handler",
                    "failed during the merge action on {}: {:?}",
                    mr_info.merge_request.url,
                    err,
                );

                let msg = format!(
                    "error occurred during merge action (@{}): {}",
                    data.action_data.project.maintainers().join(" @"),
                    err,
                );
                effects.push(Effect::error(msg));
            },
        }

        effects
    }
}

#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::thread;

    use git_workarea::GitContext;
    use itertools::Itertools;
    use serde_json::{json, Value};

    use crate::handlers::test::*;

    fn check_commit_message(ctx: &GitContext, branch: &str, expected: &str) {
        let cat_file = ctx
            .git()
            .arg("cat-file")
            .arg("-p")
            .arg(format!("refs/heads/{}", branch))
            .output()
            .unwrap();
        assert!(cat_file.status.success());
        let actual = String::from_utf8_lossy(&cat_file.stdout)
            .splitn(2, "\n\n")
            .skip(1)
            .join("\n\n");
        assert_eq!(actual, expected);
    }

    fn check_merge_commit(ctx: &GitContext, branch: &str, topic: &str, expected: &str) {
        check_commit_message(ctx, branch, expected);

        let log = ctx
            .git()
            .arg("log")
            .arg("--pretty=%an%n%ae%n%cn%n%ce%n%P")
            .arg(format!("refs/heads/{}", branch))
            .output()
            .unwrap();
        assert!(log.status.success());
        let lines = String::from_utf8_lossy(&log.stdout);
        let lines = lines.lines().collect::<Vec<_>>();

        let author_name = lines[0];
        assert_eq!(author_name, "Ghostflow Director");

        let author_email = lines[1];
        assert_eq!(author_email, "ghostflow-director-testing@example.com");

        let committer_name = lines[2];
        assert_eq!(committer_name, "Ghostflow Director");

        let committer_email = lines[3];
        assert_eq!(committer_email, "ghostflow-director@example.com");

        let parents = lines[4].split_whitespace().collect::<Vec<_>>();
        assert_eq!(parents.len(), 2);
        assert_eq!(parents[1], topic);
    }

    fn check_ff_merge_commit(ctx: &GitContext, branch: &str, topic: &str) {
        let rev_parse = ctx
            .git()
            .arg("rev-parse")
            .arg(format!("refs/heads/{}", branch))
            .output()
            .unwrap();
        assert!(rev_parse.status.success());
        let lines = String::from_utf8_lossy(&rev_parse.stdout);
        assert_eq!(lines.trim(), topic);
    }

    fn check_into_merge_commit(ctx: &GitContext, branch: &str, from_branch: &str) {
        let commit_msg = format!("Merge branch '{}' into {}", from_branch, branch);
        let rev_parse = ctx
            .git()
            .arg("rev-parse")
            .arg(format!("refs/heads/{}", from_branch))
            .output()
            .unwrap();
        assert!(rev_parse.status.success());
        let commit = String::from_utf8_lossy(&rev_parse.stdout);

        check_merge_commit(ctx, branch, commit.trim(), &commit_msg);

        let diff_tree = ctx
            .git()
            .arg("diff-tree")
            .arg("--exit-code")
            .arg("--quiet")
            .arg(format!("refs/heads/{}~", branch))
            .arg(format!("refs/heads/{}", branch))
            .output()
            .unwrap();
        assert!(diff_tree.status.success());
    }

    fn commit_status_description(commit: &str) -> String {
        format!(
            "overall branch status for the content checks against master\n\nBranch-at: {}",
            commit,
        )
    }

    const MERGE_REQUEST_TOPIC: &str = "6f781d4d4d85dfd5a609532a26bcc6fd63fcef51";
    const MERGE_REQUEST_FF_TOPIC: &str = "2f3fb3415731014d137f5be7b68e42a2cf09b3ed";
    const MERGE_REQUEST_TOPIC_CONFLICT: &str = "4c470b2665edb1c41aa09a8b54aaea21347a0093";
    const BRANCH_NEXT: &str = "27485b5013bf65f5bccc17de921f67c069d43423";
    const BRANCH_PU: &str = "f09d82fe92ad977c5027b17c6bee3973e1241e77";

    fn test_merge_config(branches_config: &Value) -> Value {
        json!({
            "ghostflow/example": {
                "branches": branches_config,
            },
        })
    }

    fn merge_action_config() -> Value {
        json!({
            "required_access_level": "maintainer",
        })
    }

    fn merge_action_config_ff() -> Value {
        json!({
            "required_access_level": "maintainer",
            "required_access_level_ff": "owner",
        })
    }

    fn merge_branch_config(branch: &str) -> Value {
        json!({
            branch: {
                "merge": merge_action_config(),
            },
        })
    }

    fn merge_branch_config_ff(branch: &str) -> Value {
        json!({
            branch: {
                "merge": merge_action_config_ff(),
            },
        })
    }

    #[test]
    fn test_command_merge_refuse_not_configured() {
        let mut service = TestService::new(
            "test_command_merge_refuse_not_configured",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&json!({
            "master": {},
        }));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `merge` command: the command is not supported"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_merge_refuse_ff_not_configured() {
        let mut service = TestService::new(
            "test_command_merge_refuse_ff_not_configured",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "Fast-forward: true", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&json!({
            "master": {},
        }));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `merge` command: the command is not supported"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_merge_disallowed() {
        let mut service = TestService::new(
            "test_command_merge_disallowed",
            [
                Action::create_user("fork"),
                Action::create_user("unrelated"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("unrelated", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&merge_branch_config("master"));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `merge` command: permission denied: insufficient access \
                   level (Maintainer)"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_merge_ff_disallowed() {
        let mut service = TestService::new(
            "test_command_merge_ff_disallowed",
            [
                Action::create_user("fork"),
                Action::create_user("maintainer"),
                Action::new_project("ghostflow", "example"),
                Action::add_team_member("ghostflow/example", "maintainer", AccessLevel::Maintainer),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "Fast-forward: true", false),
                Action::mr_comment("maintainer", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&merge_branch_config_ff("master"));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `merge` command: permission denied: insufficient access \
                   level (Owner)"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_merge_submitter_disallowed() {
        let mut service = TestService::new(
            "test_command_merge_submitter_disallowed",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("fork", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&merge_branch_config("master"));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `merge` command: permission denied: insufficient access \
                   level (Maintainer)"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_merge_bad_arguments() {
        let mut service = TestService::new(
            "test_command_merge_bad_arguments",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: merge --unrecognized"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&merge_branch_config("master"));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `merge` command: unrecognized arguments: error: \
                   unexpected argument '--unrecognized' found"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_merge_ok() {
        let mut service = TestService::new(
            "test_command_merge_ok",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&merge_branch_config("master"));
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_merge_commit(
            &ctx,
            "master",
            MERGE_REQUEST_TOPIC,
            "Merge topic 'mr-source'\n\
             \n\
             6f781d4 fork-master: add a file\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Merge-request: !0\n",
        );
    }

    #[test]
    fn test_command_merge_ff_ok() {
        let mut service = TestService::new(
            "test_command_merge_ff_ok",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_FF_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "Fast-forward: true", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&merge_branch_config_ff("master"));
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_ff_merge_commit(&ctx, "master", MERGE_REQUEST_FF_TOPIC);
    }

    #[test]
    fn test_command_merge_ff_not_ancestor() {
        let mut service = TestService::new(
            "test_command_merge_ff_not_ancestor",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "Fast-forward: true", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&merge_branch_config_ff("master"));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 This merge request may not be merged into `master` because a fast-forward merge \
                 was requested, but is not possible because the branch is no longer an ancestor."
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_merge_missing_checks() {
        let mut service = TestService::new(
            "test_command_merge_missing_checks",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                // This will cause the checks to not be run at all.
                Action::create_mr("ghostflow", "example", "fork", "Backport: next", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&merge_branch_config("master"));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `merge` command: refusing to merge; topic is missing the \
                   checks"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_merge_failing_checks() {
        let mut service = TestService::new(
            "test_command_merge_failing_checks",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&json!({
            "master": {
                "checks": {
                    "changelog": {
                        "kind": "changelog/topic",
                        "config": {
                            "style": "file",
                            "path": "CHANGELOG.md",
                            "required": true,
                        },
                    },
                },
                "merge": merge_action_config(),
            },
        }));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `merge` command: refusing to merge; topic is failing the \
                   checks"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_merge_failing_backport_checks() {
        let mut service = TestService::new(
            "test_command_merge_failing_backport_checks",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "Backport: next", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&json!({
            "master": {
                "merge": merge_action_config(),
            },
            "next": {
                "checks": {
                    "changelog": {
                        "kind": "changelog/topic",
                        "config": {
                            "style": "file",
                            "path": "CHANGELOG.md",
                            "required": true,
                        },
                    },
                },
                "merge": merge_action_config(),
            },
        }));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `merge` command: refusing to merge; topic is failing the \
                   checks"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_merge_spoofed_checks_user() {
        let mut service = TestService::new(
            "test_command_merge_spoofed_checks_user",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                // This will cause the checks to not be run at all.
                Action::create_mr("ghostflow", "example", "fork", "Backport: next", false),
                Action::create_status(
                    "fork",
                    "ghostflow-check-master",
                    &commit_status_description(MERGE_REQUEST_TOPIC),
                    None,
                    CommitStatusState::Success,
                ),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&merge_branch_config("master"));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `merge` command: refusing to merge; topic is missing the \
                   checks"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_merge_spoofed_checks_commit() {
        let mut service = TestService::new(
            "test_command_merge_spoofed_checks_commit",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                // This will cause the checks to not be run at all.
                Action::create_mr("ghostflow", "example", "fork", "Backport: next", false),
                Action::create_status(
                    "ghostflow",
                    "ghostflow-check-master",
                    &commit_status_description(MERGE_REQUEST_TOPIC_CONFLICT),
                    None,
                    CommitStatusState::Success,
                ),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&merge_branch_config("master"));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `merge` command: refusing to merge; topic is missing the \
                   checks"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_merge_spoofed_backport_checks_user() {
        let mut service = TestService::new(
            "test_command_merge_missing_checks_user",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                // This will cause the checks to not be run at all.
                Action::create_mr("ghostflow", "example", "fork", "Backport: next", false),
                Action::create_status(
                    "fork",
                    "ghostflow-check-master",
                    &commit_status_description(MERGE_REQUEST_TOPIC),
                    None,
                    CommitStatusState::Success,
                ),
                Action::create_status(
                    "fork",
                    "ghostflow-check-next",
                    &commit_status_description(MERGE_REQUEST_TOPIC),
                    None,
                    CommitStatusState::Success,
                ),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&merge_branch_config("master"));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `merge` command: refusing to merge; topic is missing the \
                   checks"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_merge_spoofed_backport_checks_commit() {
        let mut service = TestService::new(
            "test_command_merge_spoofed_backport_checks_commit",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                // This will cause the checks to not be run at all.
                Action::create_mr("ghostflow", "example", "fork", "Backport: next", false),
                Action::create_status(
                    "ghostflow",
                    "ghostflow-check-master",
                    &commit_status_description(MERGE_REQUEST_TOPIC_CONFLICT),
                    None,
                    CommitStatusState::Success,
                ),
                Action::create_status(
                    "ghostflow",
                    "ghostflow-check-next",
                    &commit_status_description(MERGE_REQUEST_TOPIC_CONFLICT),
                    None,
                    CommitStatusState::Success,
                ),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&merge_branch_config("master"));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `merge` command: refusing to merge; topic is missing the \
                   checks"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_merge_wip() {
        let mut service = TestService::new(
            "test_command_merge_wip",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "WIP", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&merge_branch_config("master"));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 This merge request is marked as a Work in Progress and may not be merged. Please \
                 remove the Work in Progress state first."
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_merge_same_name_branch() {
        let mut create_mr = Action::create_mr("ghostflow", "example", "fork", "", false);
        if let Action::CreateMergeRequest {
            ref mut source_branch,
            ..
        } = create_mr
        {
            *source_branch = "master".into();
        } else {
            panic!("expected a CreateMergeRequest action");
        };
        let mut service = TestService::new(
            "test_command_merge_same_name_branch",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "master", MERGE_REQUEST_TOPIC),
                create_mr,
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&merge_branch_config("master"));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `merge` command: the name of the topic (`master`) is the \
                   same as a branch in the project; the `--topic` option may be used to merge \
                   with a different name"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_merge_same_name_branch_other() {
        let mut create_mr = Action::create_mr("ghostflow", "example", "fork", "", false);
        if let Action::CreateMergeRequest {
            ref mut source_branch,
            ..
        } = create_mr
        {
            *source_branch = "release".into();
        } else {
            panic!("expected a CreateMergeRequest action");
        };
        let mut service = TestService::new(
            "test_command_merge_same_name_branch_other",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "release", MERGE_REQUEST_TOPIC),
                Action::push("fork", "example", "release", MERGE_REQUEST_TOPIC),
                create_mr,
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&json!({
            "master": {
                "merge": merge_action_config(),
            },
            "release": {},
        }));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `merge` command: the name of the topic (`release`) is the \
                   same as a branch in the project; the `--topic` option may be used to merge \
                   with a different name"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_merge_same_name_branch_renamed() {
        let mut service = TestService::new(
            "test_command_merge_same_name_branch_renamed",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: merge --topic master"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&merge_branch_config("master"));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `merge` command: the name of the topic (`master`) is the \
                   same as a branch in the project; the `--topic` option may be used to merge \
                   with a different name"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_merge_rename_via_description() {
        let mut service = TestService::new(
            "test_command_merge_rename_via_description",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr(
                    "ghostflow",
                    "example",
                    "fork",
                    "Topic-rename: renamed",
                    false,
                ),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&merge_branch_config("master"));
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_merge_commit(
            &ctx,
            "master",
            MERGE_REQUEST_TOPIC,
            "Merge topic 'renamed'\n\
             \n\
             6f781d4 fork-master: add a file\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Merge-request: !0\n",
        );
    }

    #[test]
    fn test_command_merge_rename_via_argument() {
        let mut service = TestService::new(
            "test_command_merge_rename_via_argument",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: merge --topic renamed"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&merge_branch_config("master"));
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_merge_commit(
            &ctx,
            "master",
            MERGE_REQUEST_TOPIC,
            "Merge topic 'renamed'\n\
             \n\
             6f781d4 fork-master: add a file\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Merge-request: !0\n",
        );
    }

    #[test]
    fn test_command_merge_push_failed() {
        let mut service = TestService::new(
            "test_command_merge_push_failed",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::delay_director("update-master", 500),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&merge_branch_config("master"));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `merge` command: failed to push the resulting merge"
                .into(),
        };

        let project_subdir = "projects/ghostflow/example.git";
        let project_dir = service.root().join(project_subdir);
        let refpath = "refs/heads/master";
        let clone_indicator = service
            .root()
            .join("director")
            .join(project_subdir)
            .join(refpath);
        let delay_path = service.root().join("delay").join("update-master");
        let ctx = GitContext::new(project_dir);

        let update_ref_thread = thread::spawn(move || {
            // Wait for the project to be cloned.
            loop {
                if clone_indicator.exists() {
                    break;
                }
            }

            // Emulate a push that has happened in the repository that has not made a notification yet.
            let update_ref = ctx
                .git()
                .arg("update-ref")
                .arg("refs/heads/master")
                .arg(BRANCH_NEXT)
                .output()
                .map_err(|_| "failed to create update-ref command")?;
            if !update_ref.status.success() {
                return Err(format!(
                    "failed to update master to point to next: {}",
                    String::from_utf8_lossy(&update_ref.stderr),
                ));
            }

            File::create(delay_path).map_err(|_| "failed to create the delay file")?;

            Ok(())
        });

        service.launch(config, end).unwrap();
        update_ref_thread.join().unwrap().unwrap();
    }

    #[test]
    fn test_command_merge_ff_push_failed() {
        let mut service = TestService::new(
            "test_command_merge_ff_push_failed",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "Fast-forward: true", false),
                Action::delay_director("update-master", 500),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&merge_branch_config_ff("master"));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 This merge request may not be merged into `master` because a fast-forward merge \
                 was requested, but is not possible because the branch is no longer an ancestor."
                .into(),
        };

        let project_subdir = "projects/ghostflow/example.git";
        let project_dir = service.root().join(project_subdir);
        let refpath = "refs/heads/master";
        let clone_indicator = service
            .root()
            .join("director")
            .join(project_subdir)
            .join(refpath);
        let delay_path = service.root().join("delay").join("update-master");
        let ctx = GitContext::new(project_dir);

        let update_ref_thread = thread::spawn(move || {
            // Wait for the project to be cloned.
            loop {
                if clone_indicator.exists() {
                    break;
                }
            }

            // Emulate a push that has happened in the repository that has not made a notification yet.
            let update_ref = ctx
                .git()
                .arg("update-ref")
                .arg("refs/heads/master")
                .arg(BRANCH_NEXT)
                .output()
                .map_err(|_| "failed to create update-ref command")?;
            if !update_ref.status.success() {
                return Err(format!(
                    "failed to update master to point to next: {}",
                    String::from_utf8_lossy(&update_ref.stderr),
                ));
            }

            File::create(delay_path).map_err(|_| "failed to create the delay file")?;

            Ok(())
        });

        service.launch(config, end).unwrap();
        update_ref_thread.join().unwrap().unwrap();
    }

    #[test]
    fn test_command_merge_conflicts() {
        let mut service = TestService::new(
            "test_command_merge_conflicts",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC_CONFLICT),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&merge_branch_config("master"));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 This merge request contains conflicts with `master` in the following paths:\n\n  \
                 - `master`"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_merge_backports() {
        let mut service = TestService::new(
            "test_command_merge_backports",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "Backport: next", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&json!({
            "master": {
                "merge": merge_action_config(),
            },
            "next": {
                "merge": merge_action_config(),
            },
        }));
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_merge_commit(
            &ctx,
            "master",
            MERGE_REQUEST_TOPIC,
            "Merge topic 'mr-source'\n\
             \n\
             6f781d4 fork-master: add a file\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Merge-request: !0\n",
        );
        check_merge_commit(
            &ctx,
            "next",
            MERGE_REQUEST_TOPIC,
            "Merge topic 'mr-source' into next\n\
             \n\
             6f781d4 fork-master: add a file\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Merge-request: !0\n",
        );
    }

    #[test]
    fn test_command_merge_backports_no_merge() {
        let mut service = TestService::new(
            "test_command_merge_backports_no_merge",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "Backport: next", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&json!({
            "master": {
                "merge": merge_action_config(),
            },
            "next": {},
        }));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `merge` command: error occurred during merge action \
                   (@ghostflow): the `next` branch does not support merging (with the specified \
                   merge topology)"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_merge_backports_conflict() {
        let mut service = TestService::new(
            "test_command_merge_backports_conflict",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC_CONFLICT),
                Action::create_mr("ghostflow", "example", "fork", "Backport: next", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&json!({
            "master": {
                "merge": merge_action_config(),
            },
            "next": {
                "merge": merge_action_config(),
            },
        }));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 This merge request contains conflicts with `next` in the following paths:\n\n  \
                 - `master`"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_merge_into() {
        let mut service = TestService::new(
            "test_command_merge_into",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&json!({
            "master": {
                "merge": merge_action_config(),
                "into_branches": [
                    "next",
                ],
            },
            "next": {},
        }));
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_merge_commit(
            &ctx,
            "master",
            MERGE_REQUEST_TOPIC,
            "Merge topic 'mr-source'\n\
             \n\
             6f781d4 fork-master: add a file\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Merge-request: !0\n",
        );
        check_into_merge_commit(&ctx, "next", "master");
    }

    #[test]
    fn test_command_merge_ff_into() {
        let mut service = TestService::new(
            "test_command_merge_ff_into",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_FF_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "Fast-forward: true", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&json!({
            "master": {
                "merge": merge_action_config_ff(),
                "into_branches": [
                    "next",
                ],
            },
            "next": {},
        }));
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_ff_merge_commit(&ctx, "master", MERGE_REQUEST_FF_TOPIC);
        check_into_merge_commit(&ctx, "next", "master");
    }

    #[test]
    fn test_command_merge_into_many() {
        let mut service = TestService::new(
            "test_command_merge_into_many",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
                Action::push("ghostflow", "example", "pu", BRANCH_PU),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&json!({
            "master": {
                "merge": merge_action_config(),
                "into_branches": [
                    "next",
                    "pu",
                ],
            },
            "next": {},
            "pu": {},
        }));
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_merge_commit(
            &ctx,
            "master",
            MERGE_REQUEST_TOPIC,
            "Merge topic 'mr-source'\n\
             \n\
             6f781d4 fork-master: add a file\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Merge-request: !0\n",
        );
        check_into_merge_commit(&ctx, "next", "master");
        check_into_merge_commit(&ctx, "pu", "master");
    }

    #[test]
    fn test_command_merge_into_chain() {
        let mut service = TestService::new(
            "test_command_merge_into_chain",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
                Action::push("ghostflow", "example", "pu", BRANCH_PU),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&json!({
            "master": {
                "merge": merge_action_config(),
                "into_branches": [
                    "next",
                ],
            },
            "next": {
                "into_branches": [
                    "pu",
                ],
            },
            "pu": {},
        }));
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_merge_commit(
            &ctx,
            "master",
            MERGE_REQUEST_TOPIC,
            "Merge topic 'mr-source'\n\
             \n\
             6f781d4 fork-master: add a file\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Merge-request: !0\n",
        );
        check_into_merge_commit(&ctx, "next", "master");
        check_into_merge_commit(&ctx, "pu", "next");
    }

    #[test]
    fn test_command_merge_into_with_backports() {
        let mut service = TestService::new(
            "test_command_merge_into_with_backports",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "Backport: next", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&json!({
            "master": {
                "merge": merge_action_config(),
                "into_branches": [
                    "next",
                ],
            },
            "next": {
                "merge": merge_action_config(),
            },
        }));
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_merge_commit(
            &ctx,
            "master",
            MERGE_REQUEST_TOPIC,
            "Merge topic 'mr-source'\n\
             \n\
             6f781d4 fork-master: add a file\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Merge-request: !0\n",
        );
        check_into_merge_commit(&ctx, "next", "master");
        check_merge_commit(
            &ctx,
            "next~",
            MERGE_REQUEST_TOPIC,
            "Merge topic 'mr-source' into next\n\
             \n\
             6f781d4 fork-master: add a file\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Merge-request: !0\n",
        );
    }

    #[test]
    fn test_command_merge_ff_into_with_backports() {
        let mut service = TestService::new(
            "test_command_merge_ff_into_with_backports",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_FF_TOPIC),
                Action::create_mr(
                    "ghostflow",
                    "example",
                    "fork",
                    "Fast-forward: true\nBackport: next",
                    false,
                ),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&json!({
            "master": {
                "merge": merge_action_config_ff(),
                "into_branches": [
                    "next",
                ],
            },
            "next": {
                "merge": merge_action_config(),
            },
        }));
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_ff_merge_commit(&ctx, "master", MERGE_REQUEST_FF_TOPIC);
        check_merge_commit(
            &ctx,
            "next",
            MERGE_REQUEST_FF_TOPIC,
            "Merge topic 'mr-source' into next\n\
             \n\
             2f3fb34 master: add a line\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Merge-request: !0\n",
        );
    }

    #[test]
    fn test_command_merge_ff_into_with_backports_flip() {
        let mut service = TestService::new(
            "test_command_merge_ff_into_with_backports_flip",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_FF_TOPIC),
                {
                    let mut mr = Action::create_mr(
                        "ghostflow",
                        "example",
                        "fork",
                        "Backport-ff: master",
                        false,
                    );
                    if let Action::CreateMergeRequest {
                        ref mut target_branch,
                        ..
                    } = mr
                    {
                        *target_branch = "next".into();
                    }
                    mr
                },
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config(&json!({
            "master": {
                "merge": merge_action_config_ff(),
                "into_branches": [
                    "next",
                ],
            },
            "next": {
                "merge": merge_action_config(),
            },
        }));
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_ff_merge_commit(&ctx, "master", MERGE_REQUEST_FF_TOPIC);
        check_merge_commit(
            &ctx,
            "next",
            MERGE_REQUEST_FF_TOPIC,
            "Merge topic 'mr-source' into next\n\
             \n\
             2f3fb34 master: add a line\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Merge-request: !0\n",
        );
    }
}
