#!/usr/bin/env python3
'''
Create a merge request bypass token.

This scripts generates a bypass token to allow a merge request to be merged
without considering the CI results.

The only required flags are:

  - `-s`: the secret for the project
  - `-p`: the project which hosts the merge request
  - `-m`: the ID of the merge request

Optional flags include:

  - `-g`: the GitLab instance to communicate with (defaults to
    `gitlab.kitware.com`)
  - `-t`: the GitLab token (required for private projects)
'''

import argparse
import hashlib
import pathlib
import re
import requests


def options():
    p = argparse.ArgumentParser(
        description='Create a merge request for the release process')
    p.add_argument('-s', '--secret', type=str, required=True,
                   help='the secret for the project')
    p.add_argument('-g', '--gitlab', type=str, default='gitlab.kitware.com',
                   help='the gitlab instance to communicate with')
    p.add_argument('-p', '--project', type=str, required=True,
                   help='the project to generate a token for')
    p.add_argument('-m', '--merge-request', type=int, required=True,
                   help='the merge request to generate a token for')
    p.add_argument('-r', '--repo', type=pathlib.Path,
                   help='the path to a local clone of the repository; required for relative backports')
    p.add_argument('-t', '--token', type=str,
                   help='API token to communicate for GitLab (for private projects)')
    return p


GHOSTFLOW_BYPASS_SALT = 'ghostflow-director-bypass-token'
GHOSTFLOW_BYPASS_PREFIX = 'gdbt-'
TRAILER_RE = re.compile(''.join([
    '^',
    '(?P<token>[a-zA-Z-]+)',
    ':\\s+',
    '(?P<value>.+?)',
    '\\s*',
    '$',
]))


class Gitlab(object):
    def __init__(self, gitlab_url, token):
        self.gitlab_url = f'https://{gitlab_url}/api/v4'
        self.headers = {}
        if token is not None:
            self.headers['PRIVATE-TOKEN'] = token

    def get(self, endpoint, **kwargs):
        rsp = requests.get(f'{self.gitlab_url}/{endpoint}',
                           headers=self.headers,
                           params=kwargs)

        if rsp.status_code != 200:
            raise RuntimeError(f'Failed request to {endpoint}: {rsp.content}')

        return rsp.json()


def resolve_ref(repo, instance, project, mr, ref, token=None):
    import os.path
    import subprocess

    if not repo:
        raise RuntimeError(f'Repository path is required when relative backports are provided')

    if not os.path.isdir(repo):
        raise RuntimeError(f'Repository path {repo} does not exist')

    protocol = 'https'
    creds = ''

    if token is not None:
        protocol = 'ssh'
        creds = 'git@'

    url =f'{protocol}://{creds}{instance}/{project}.git'
    refname = f'refs/merge-requests/{mr}/head'

    subprocess.check_call([
            'git',
            'fetch',
            url,
            refname,
        ],
        stdin=None,
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
        cwd=repo)

    revparse = subprocess.run([
            'git',
            'rev-parse',
            '--verify',
            f'{ref}^{{commit}}',
        ],
        stdin=None,
        capture_output=True,
        cwd=repo)
    revparse.check_returncode()
    return revparse.stdout.strip().decode('utf-8')


if __name__ == '__main__':
    p = options()
    opts = p.parse_args()

    gitlab = Gitlab(opts.gitlab, opts.token)

    project_safe = opts.project.replace('/', '%2f')
    mr = gitlab.get(f'projects/{project_safe}/merge_requests/{opts.merge_request}')

    url = mr['web_url']
    commit = mr['sha']
    target_branch = mr['target_branch']
    description = mr['description']

    backports = {}
    main_ff = False

    desc_lines = description.split('\n')
    desc_lines.reverse()
    for line in desc_lines:
        if not line:
            continue
        matches = TRAILER_RE.match(line)
        if not matches:
            break
        token = matches['token']
        value = matches['value']
        if token in ('Fast-forward', 'fast-forward') and value == 'true':
            main_ff = True
            continue
        ff = False
        if token in ('Backport-ff', 'backport-ff'):
            ff = True
        elif token not in ('Backport', 'backport'):
            continue
        if ':' in value:
            b, cref = value.split(':', maxsplit=1)
            cref = cref.replace('HEAD', commit)
            cref = resolve_ref(opts.repo, opts.gitlab, opts.project, opts.merge_request, cref, token=opts.token)
            backports[b] = (cref, ff)
        else:
            backports[value] = (commit, ff)

    backports[target_branch] = (commit, main_ff)

    m = hashlib.sha256()
    m.update(GHOSTFLOW_BYPASS_SALT.encode('utf-8'))
    m.update(b'::')
    m.update(url.encode('utf-8'))
    m.update(b'::')
    for b, i in sorted(backports.items(), key=lambda b: b[0]):
        c = i[0]
        ff = i[1]
        if ff:
            strategy = 'F'
        else:
            strategy = 'M'
        m.update(b.encode('utf-8'))
        m.update(b'@')
        m.update(strategy.encode('utf-8'))
        m.update(c.encode('utf-8'))
        m.update(b'::')
    m.update(opts.secret.encode('utf-8'))

    print(f'{GHOSTFLOW_BYPASS_PREFIX}{m.hexdigest()}')
