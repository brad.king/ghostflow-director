% Workflow

# Workflow

The workflow that this crate's actions are generally designed around is a
fork-based workflow where all contributors (including maintainers) work in a
fork of the main repository. Maintainers and developers may have special rights
in the main repository, but topics are not pushed to it directly.

The rationale for everybody keeping their own forks is that all branches and
topics in the main repository are copied to forks when they are created. In
general, contributors do not clean up unnecessary topics when creating a fork
which can cause confusion as to which topics are active within a fork.

# Glossary

  - *project*: A main repository as well as all of its forks.
  - *fork*: A user-maintained copy of the main repository where the user has
    full rights within it regardless of their rights on the main repository.
  - *branch*: An integration branch for development, testing, or release
    maintenance. Ideally, the main ancestry of a branch should consist solely
    of merge commits of topics, but this may not always be the case.
  - *topic*: A branch intended to be integrated into a branch in the main
    repository. Generally topics should be focused on a single goal.
  - *merge request*: A request for review by maintainers and other developers
    of a topic into a branch. Merge requests may be called differently
    depending on the service in use, but are referred to as "merge requests" in
    this tool.
