#!/usr/bin/env python3

from datetime import datetime
import json
import os
import statistics
import sys


FILENAME_FORMAT = '%Y-%m-%dT%H:%M:%S.%f'
STAMP_FORMAT = '%Y-%m-%d %H:%M:%S.%f'
GITLAB_FORMAT = '%Y-%m-%dT%H:%M:%SZ'
HOOK_FORMAT = '%Y-%m-%d %H:%M:%S UTC'


def collect_times(basedir, filenames, gitlab_delays, webhook_delays):
    for filename in filenames:
        if not filename.endswith('.json'):
            continue

        with open(os.path.join(basedir, filename), 'r') as fin:
            blob = json.load(fin)

        stampfile = filename[:-5] + '.stamp'
        if filename.startswith('20'):
            drop = '-'.join(filename.split('-')[:3])
            drop_time = datetime.strptime(drop[:-4], FILENAME_FORMAT)
        else:
            drop_time = datetime.utcfromtimestamp(os.stat(os.path.join(basedir, filename)).st_mtime)
        with open(os.path.join(basedir, stampfile), 'r') as fin:
            stamp = fin.read().strip()
            done_time = datetime.strptime(stamp[:-7], STAMP_FORMAT)

        kind = blob['kind']
        data = blob['data']
        if kind == 'gitlab:project_create':
            gitlab_time = datetime.strptime(data['created_at'], GITLAB_FORMAT)
        elif kind == 'gitlab:merge_request':
            gitlab_time = datetime.strptime(data['object_attributes']['updated_at'], HOOK_FORMAT)
        elif kind == 'gitlab:note':
            gitlab_time = datetime.strptime(data['object_attributes']['created_at'], HOOK_FORMAT)
        elif kind == 'gitlab:user_add_to_team':
            gitlab_time = datetime.strptime(data['created_at'], GITLAB_FORMAT)
        elif kind == 'gitlab:push':
            # No time available
            gitlab_time = None
        elif kind == 'gitlab:unknown':
            if 'event_name' in data:
                event_name = data['event_name']
                if event_name == 'user_create':
                    gitlab_time = datetime.strptime(data['created_at'], GITLAB_FORMAT)
                elif event_name == 'key_create':
                    gitlab_time = datetime.strptime(data['created_at'], GITLAB_FORMAT)
        elif kind == 'watchdog:restart':
            gitlab_time = None
        else:
            sys.stderr.write('unknown kind: %s\n' % kind)
            gitlab_time = None

        if gitlab_time:
            gitlab_delays.append((kind, (drop_time - gitlab_time).total_seconds()))
        webhook_delays.append((kind, (done_time - drop_time).total_seconds()))


def print_time_stats(label, times):
    mean = statistics.mean(times)
    median = statistics.median(times)
    if len(times) > 1:
        stdev = statistics.stdev(times)
    else:
        stdev = None

    print(label)
    print('-' * len(label))
    print('min:    {}'.format(min(times)))
    print('max:    {}'.format(max(times)))
    print('mean:   {}'.format(mean))
    print('median: {}'.format(median))
    print('stdev:  {}'.format(stdev))
    print('data:   {}'.format(times))
    print('')


def print_statistics(label, delay_list):
    all_kinds = []
    per_kind = {}

    for (kind, delay) in delay_list:
        all_kinds.append(delay)
        per_kind.setdefault(kind, []).append(delay)

    print(label)
    print('=' * len(label))
    print_time_stats('overall', all_kinds)
    for (kind, times) in per_kind.items():
        print_time_stats(kind, times)


def inspect_queue(queuedir):
    acceptdir = os.path.join(queuedir, 'accept')
    rejectdir = os.path.join(queuedir, 'reject')

    accepts = os.listdir(acceptdir)
    rejects = os.listdir(rejectdir)

    sidekiq_delays = []
    accept_delays = []
    reject_delays = []

    collect_times(acceptdir, accepts, sidekiq_delays, accept_delays)
    collect_times(rejectdir, rejects, sidekiq_delays, reject_delays)

    print_statistics('sidekiq', sidekiq_delays)
    print('')
    print_statistics('acceptance', accept_delays)
    print('')
    print_statistics('rejection', reject_delays)

if __name__ == '__main__':
    queuedir = sys.argv[1]
    inspect_queue(queuedir)
