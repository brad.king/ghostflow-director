.cargo_before_script: &cargo_before_script
    - apt-get update -yqq
    - export CARGO_HOME=.cargo-cache
    - rustc --version
    - cargo --version

.cargo_build_tags:
    tags:
        - build
        - docker
        - ghostflow
        - linux-x86_64

.cargo_privileged_tags:
    tags:
        - docker
        - ghostflow
        - linux-x86_64
        - privileged

.cargo_fetch_job:
    stage: prep

    script:
        - *cargo_before_script
        #- cargo update $GENERATE_LOCKFILE_ARGS
        - cargo fetch --locked
        - mkdir .cargo
        - cargo vendor > .cargo/config.toml
    cache:
        key: cargo-cache-$CARGO_UPDATE_POLICY
        paths:
            - .cargo-cache

    interruptible: true

.cargo_clippy_job:
    stage: build
    script:
        - *cargo_before_script
        - rustup component add clippy
        - cargo clippy --frozen --tests --all --verbose -- -D warnings
    interruptible: true

.cargo_build_job:
    stage: build
    script:
        - *cargo_before_script
        # Only use it if it's available; no need to fail the build due to
        # something gone wrong here.
        - .gitlab/ci/sccache.sh && export RUSTC_WRAPPER=$PWD/.gitlab/sccache
        - cargo build --frozen --all --verbose
        - cargo test --frozen --all --no-run --verbose
        - ".gitlab/sccache --show-stats || :"
    variables:
        CARGO_INCREMENTAL: "0"
    artifacts:
        expire_in: 24h
        paths:
            - vendor
            - .cargo
            - Cargo.lock
            - target
    interruptible: true

.cargo_test_job:
    stage: test
    script:
        - *cargo_before_script
        - .gitlab/ci/cargo-nextest.sh
        - export PATH=$PWD/.gitlab:$PATH
        - apt-get install -yqq --no-install-recommends git rsync
        - git config --global user.name "Ghostflow Testing"
        - git config --global user.email "ghostflow@example.invalid"
        - '[ -d "$PWD/git/root/bin" ] && PATH=$PWD/git/root/bin:$PATH'
        # Make a ref locally available. The `clone` test assumes that the test
        # repository has a branch checked out. This is not the case on gitlab-ci.
        - git fetch origin +refs/heads/*:refs/heads/*
        - cargo nextest run --profile ci --frozen --all --verbose
    timeout: 15m
    interruptible: true

.cargo_tarpaulin_build_job:
    stage: build
    script:
        - *cargo_before_script
        - .gitlab/ci/sccache.sh && export RUSTC_WRAPPER=$PWD/.gitlab/sccache
        - .gitlab/ci/tarpaulin.sh
        - export PATH=$PWD/.gitlab:$PATH
        - cargo tarpaulin --no-run --frozen --exclude-files 'vendor/*' --ignore-panics --all --verbose --engine llvm
        - ".gitlab/sccache --show-stats || :"
    variables:
        CARGO_INCREMENTAL: "0"
    interruptible: true

.cargo_tarpaulin_test_job:
    stage: test
    script:
        - *cargo_before_script
        - .gitlab/ci/tarpaulin.sh
        - export PATH=$PWD/.gitlab:$PATH
        - apt-get install -yqq --no-install-recommends git rsync
        - git config --global user.name "Ghostflow Testing"
        - git config --global user.email "ghostflow@example.invalid"
        # Make a ref locally available. The `clone` test assumes that the test
        # repository has a branch checked out. This is not the case on gitlab-ci.
        - git fetch origin +refs/heads/*:refs/heads/*
        - cargo tarpaulin --frozen --exclude-files 'vendor/*' --ignore-panics --all --verbose --out Html --engine llvm
    coverage: '/\d+.\d+% coverage, \d+\/\d+ lines covered/'
    interruptible: true

.cargo_audit_job:
    stage: build
    script:
        - *cargo_before_script
        - .gitlab/ci/cargo-audit.sh
        - export PATH=$PWD/.gitlab:$PATH
        - cargo audit -D warnings -d .cargo-audit-db $CARGO_AUDIT_ARGS
    cache:
        key: cargo-cache-$CARGO_UPDATE_POLICY
        paths:
            - .cargo-audit-db
    interruptible: true
