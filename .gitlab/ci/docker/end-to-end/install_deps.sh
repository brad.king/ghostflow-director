#!/bin/sh

set -e

# Build dependencies.
dnf install -y --setopt=install_weak_deps=False \
    cargo git-core openssl-devel xz-devel

# Run dependencies.
dnf install -y --setopt=install_weak_deps=False \
    rsync openssl1.1

# Test dependencies.
dnf install -y --setopt=install_weak_deps=False \
    podman-docker which jq iproute pwgen fuse-overlayfs

# Silence "podman as docker" messages.
touch /etc/containers/nodocker

# Fix some `podman` behaviors for CI purposes.
cat > /etc/containers/registries.conf.d/999-kitware-short-names.conf <<EOF
short-name-mode="disabled"
EOF

# Set up git user information.
git config --global user.name "Ghostflow Testing"
git config --global user.email "ghostflow@example.invalid"

dnf clean all
