ghostflow-director
==================

The Kitware workflow uses a robot to manage various actions required for them.
This includes things such as running checks on topic branches, merging topic
branches, synchronizing mirrors of repositories, and handling test actions for
projects.

This tool is that robot, called "ghostflow-director" because it directs a
git-hosted workflow.

Projects are configured to be managed by various parts of the workflow which
are then handled by listening to JSON job files placed in a directory watched
by the robot. The job files are sorted out based on whether they were accepted
or rejected (e.g., a project might send events, but no actions are
configured).
